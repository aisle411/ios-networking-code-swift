//
//  IVKSearchItem.swift
//  VenueKit
//
//  Created by Fred Priese on 10/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

///Class defining a search item in a search list.
@objc
public class IVKSearchItem: IVKObject, NSCopying {
    ///Item name, descriptor or type.
    ///e.g. coke, coca-cola, cola, soda, beverage
    @objc
    public var term: String = ""
    ///UPC to be used in the search
    @objc
    public var upc: String = ""
    ///Array of UPCs that are related to term or upc.
    @objc
    public var upcs: [String] = []
    ///Category as set up when the venue/partner was set up.
    @objc
    public var category: String = ""
    ///Dictionary representation consistent with the legacy shopping list search.
    @objc
    public var dictionary: [String: AnyObject]? {
        get {
            var dict: [String: AnyObject] = [:]
            if self.term.length > 0 {
                dict["name"] = self.term as AnyObject
            }
            if self.upc.length > 0 {
                dict["upc"] = self.upc as AnyObject
            }
            if self.category.length > 0 {
                dict["category_name"] = self.category as AnyObject
            }
            if self.upcs.count > 0 {
                dict["upcs"] = self.upcs as AnyObject
            }
            if dict.count == 0 {
                return nil
            }
            return dict
        }
    }
    
    /// Init with an array of UPCs
    @objc
    public init(upcs: [String]) {
        self.upcs = upcs
    }
    
    /// Init with a UPC.
    @objc
    public init(upc: String) {
        self.upc = upc
    }
    
    /// Init with a term and optionally a category.
    @objc
    public init(term: String, category: String = "") {
        self.term = term
        self.category = category
    }
    
    /// Init with a term, array of UPCs and optionally a category.
    @objc
    public init(term: String, upcs: [String], category: String = "") {
        self.term = term
        self.upcs = upcs
        self.category = category
    }
    
    @objc
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = IVKSearchItem(upc: self.upc.copy() as! String)
        copy.term = self.term.copy() as! String
        copy.category = self.category.copy() as! String
        for upc in self.upcs {
            copy.upcs.append(upc.copy() as! String)
        }
        return copy as Any
    }
}
