import Foundation

public class IVKSublocation: IVKObject {

    public              var mapId: Int? = nil
    public private(set) var locationId: Int = 0
    public private(set) var locationName: String = ""
    public private(set) var sublocationId: Int = 0
    public private(set) var sublocationName: String = ""
 
    internal init(dict: [NSString: AnyObject]) {
        if let tmp = dict["map_id"] as? Int {
            self.mapId = tmp
        }
        
        if let tmp = dict["item_location_id"] as? Int {
            self.locationId = tmp
        }
        
        if let tmp = dict["sub_location_id"] as? Int {
            self.sublocationId = tmp
        }
        
        if let tmp = dict["location_name"] as? String {
            self.locationName = tmp
        }
        
        if let tmp = dict["sub_location_name"] as? String {
            self.sublocationName = tmp
        }

    }
}
