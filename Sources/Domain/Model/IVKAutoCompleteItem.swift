//
//  IVKAutoCompleteItem.swift
//  VenueKit
//
//  Created by Minxin Guo on 2/10/17.
//  Copyright © 2017 Aisle411. All rights reserved.
//

import UIKit

public class IVKAutoCompleteItem: IVKObject {
    @objc
    private(set) public var query: String = ""
    
    @objc
    private(set) public var boldRange: [Any] = [Any]()
    
    @objc
    private(set) public var superDepartment: [String] = [String]()

    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKAutoCompleteItem {\n"
            desc += "\tquery: \(self.query)\n"
            desc += "\tboldRange: \(self.boldRange)\n"
            desc += "\tsuperDepartment: \(self.superDepartment)\n"
            desc += "}\n"
            return desc
        }
    }
    
    init(dict: [String: AnyObject], ordinal: Int?) {
        super.init()

        if let tmp = dict["query"] as? String {
            self.query = tmp
        }
        
        if let tmp = dict["bold_ranges"] as? [Any] {
            self.boldRange = tmp
        }
        
        if let tmp = dict["super_deps"] as? [String] {
            self.superDepartment = tmp
        }
    }
}
