//
//  IVKVenue.swift
//  SKit
//
//  Created by Fred Priese on 4/3/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif
import CoreLocation

/**
IVKVenue represents a venue location in the Aisle411 system.
*/
@objc
public class IVKVenue: IVKObject, NSCoding {
    // MARK: - Identity Properties
    /// Unique id across all partners.
    /// Was called retailerStoreId previously.
    @objc
    private(set) public var id: Int = 0                //retailerStoreId
    
    /// Venue name.
    @objc
    private(set) public var name: String = ""          //displayName
    
    /// Name of group or parent of the venue.
    /// Same as retailerName.
    @objc
    private(set) public var vendorName: String = ""
    
    /// Name of group or parent of the store.
    /// Same as groupName.
    /// **DEPRECATED - Use .vendorName instead.**
    @objc
    public var retailerName: String {
        get {
            return self.retailerName
        }
    }
    
    /// Display name.
    @objc
    public var displayName: String {
        get {
            var clean: String = ""
            if self.vendorName.characters.count > 0 {
                let whole: Range = self.vendorName.startIndex ..< self.vendorName.endIndex
                clean = self.vendorName.replacingOccurrences(of: "- CATEGORY", with: "", options: .caseInsensitive, range: whole)
            }
            return clean
        }
    }

    /// Id of group or parent of the venue.
    /// Same as retailerId.
    @objc
    private(set) public var vendorId: Int = 0
    
    /// Id of group or parent of the store.
    /// Same as groupId.
    /// **DEPRECATED - Use .vendorId instead.**
    @objc public var retailerId: Int {
        get {
            return self.vendorId
        }
    }
    
    /**
     Partner's venue id.
     Should be unique per partner.
     Same as vendorStoreNumber.
     */
    @objc
    private(set) public var vendorVenueNumber: Int = 0
    
    /// Partner's store id.
    /// Should be unique per partner.
    /// Same as vendorVenueNumber.
    /// **DEPRECATED - Use .vendorVenueNumber instead.**
    @objc public var vendorStoreNumber: Int {
        get {
            return self.vendorVenueNumber
        }
    }
    
    /// Aisle411 Venue Number (not the same as id.)
    @objc
    private(set) public var aisle411VenueNumber: Int = 0
    
    // MARK: - Address Properties
    /// Street address.
    @objc
    private(set) public var address1: String = ""
    
    /// Second line of street address, often used for unit (apt, ste, etc.)
    /// Not yet implemented.
    @objc
    private(set) public var address2: String = ""
    
    /// City or locale name applicable for the location, should match city name of postal code in the US
    @objc
    private(set) public var city: String = ""
    
    /// Genericized name for State or Province
    @objc
    private(set) public var region: String = ""
    
    /// Shortcut for region name for US and other countries that refer to regional political divisions as states addresses
    @objc public var state: String {
        get {
            return self.region
        }
    }
    
    /// Shortcut for region name for Candadian and other countries that refer to regional political divisions as provinces addresses
    @objc public var province: String {
        get {
            return self.region
        }
    }
    
    /// Genericized name for State or Procince Code
    @objc
    private(set) public var regionCode: String = ""
    
    /// Shortcut for region code for US addresses
    @objc public var stateCode: String {
        get {
            return self.regionCode
        }
    }
    
    
    /// Shortcut for region code for Candadian addresses
    @objc public var provinceCode: String {
        get {
            return self.regionCode
        }
    }
    
    /// The country name
    @objc
    private(set) public var country: String = ""
    
    /// The country's code
    @objc
    private(set) public var countryCode: String = ""
    
    /// Postal Code for the address, this is refered to as the ZIP code in the US.
    @objc
    private(set) public var postalCode: String = ""
    
    /// Shortcut for accessing postal code for US Addresses.
    @objc public var zipCode: String {
        get {
            return self.postalCode
        }
    }
    
    // MARK: Venue Properties
    /// Venue category
    @objc
    private(set) public var category: String = ""
    
    /// Venue Location
    @objc
    private(set) public var location: CLLocation?
    
    /// Venue Latitiude (If a location is not set this will return 0.0)
    @objc public var latitude: Double {
        get {
            if let loc = self.location {
                return loc.coordinate.latitude
            } else {
                return 0.0
            }
        }
    }
    
    /// Venue Longitude (If a location is not set this will return 0.0)
    @objc public var longitude: Double {
        get {
            if let loc = self.location {
                return loc.coordinate.longitude
            } else {
                return 0.0
            }
        }
    }
    
    /// Venue Hours
    @objc
    private(set) public var hours: String = ""
    
    /// Whether the venue has an indoor map.
    @objc
    private(set) public var mapped: Bool = false
    
    /// Whether the venue has search.
    @objc
    private(set) public var searchable: Bool = false
    
    /// Telephone number.
    @objc
    private(set) public var phone: String = ""
    
    /// Flag for future deletion.
    @objc
    private(set) public var willBeRemoved: Bool = false
    
    /// Flag for visibility of venue.
    @objc
    private(set) public var isVisible: Bool = false
    
    /// Path to logo.
    @objc
    private(set) public var logoURL: String = ""       // TODO: provide caching support
    
    /// Path to map.
    @objc
    private(set) public var mapURL: String = ""        // TODO: return map Object for usage by StoresMapKit
    
    /// Path to venue or partner website.
    @objc
    private(set) public var websiteURL: String = ""    // TODO: consider proprty returning URL
    
    
    
    // MARK: - Search Related Properties
    /// Distance from venue search origin location.
    @objc
    private(set) public var distance: Float = 0.0
    
    /// Ordinal number of venue in search results.
    @objc
    private(set) public var ordinal: Int = 0

    
    /// Description
    @objc public override var description: String {
        get {
            var desc: String = "IVKVenue {\n"
            desc += "\tid: \(self.id)\n"
            desc += "\tname: \(self.name)\n"
            desc += "\tvendorName: \(self.vendorName)\n"
            desc += "\tdisplayName: \(self.displayName)\n"
            desc += "\tvendorId: \(self.vendorId)\n"
            desc += "\tvendorVenueNumber: \(self.vendorVenueNumber)\n"
            desc += "\taisle411VenueNumber: \(self.aisle411VenueNumber)\n"
            desc += "\taddress1: \(self.address1)\n"
            desc += "\taddress2: \(self.address2)\n"
            desc += "\tcity: \(self.city)\n"
            desc += "\tregion|state|province: \(self.region)\n"
            desc += "\tregionCode|stateCode|provinceCode: \(self.regionCode)\n"
            desc += "\tcountry: \(self.country)\n"
            desc += "\tcountryCode: \(self.countryCode)\n"
            desc += "\tpostalCode|zipCode: \(self.postalCode)\n"
            desc += "\tcategory: \(self.category)\n"
            desc += "\tlocation: \(self.location)\n"
            desc += "\tlatitude: \(self.latitude)\n"
            desc += "\tlongitude: \(self.longitude)\n"
            desc += "\thours: \(self.hours)\n"
            desc += "\tmapped: \(self.mapped)\n"
            desc += "\tsearchable: \(self.searchable)\n"
            desc += "\tphone: \(self.phone)\n"
            desc += "\twillBeRemoved: \(self.willBeRemoved)\n"
            desc += "\tisVisible: \(self.isVisible)\n"
            desc += "\tlogoURL: \(self.logoURL)\n"
            desc += "\tmapURL: \(self.mapURL)\n"
            desc += "\twebsiteURL: \(self.websiteURL)\n"
            desc += "\tdistance: \(self.distance)\n"
            desc += "\tordinal: \(self.ordinal)\n"
            desc += "}\n"
            return desc
        }
    }

    internal init(dict: [NSString: AnyObject], ordinal: Int?) {
        super.init()
        // Initialize Store Identity
        if let tmp: String = dict["retailer_store_nm"] as? String {
            self.name = tmp
        }
        
        if let tmp: String = dict["retailer_nm"] as? String {
            self.vendorName = tmp
        }
        
        if let tmp: Int = dict["retailer_id"] as? Int {
            self.vendorId = tmp
        } else {
            if let tmp: NSString = dict["retailer_id"] as? NSString {
                self.vendorId = tmp.integerValue
            }
        }
        
        if let tmp: Int = dict["retailer_store_id"] as? Int {
            self.id = tmp
        } else {
            if let tmp: NSString = dict["retailer_store_id"] as? NSString {
                self.id = tmp.integerValue
            }
        }
        
        if let tmp: Int = dict["vendor_store_nbr"] as? Int {
            self.vendorVenueNumber = tmp
        } else {
            if let tmp: NSString = dict["vendor_store_nbr"] as? NSString {
                self.vendorVenueNumber = tmp.integerValue
            }
        }
        
        if let tmp: NSString = dict["aisle411_store_nbr"] as? NSString {
            self.aisle411VenueNumber = tmp.integerValue
        }
        
        // Initialize Store Address
        if let tmp: String = dict["street_address_txt"] as? String {
            self.address1 = tmp
        }
        
        if let tmp: String = dict["city_nm"] as? String {
            self.city = tmp
        }
        
        if let tmp: String = dict["state_nm"] as? String {
            self.region = tmp
        }
        
        if let tmp: String = dict["state_cd"] as? String {
            self.regionCode = tmp
        }
        
        if let tmp: String = dict["country_nm"] as? String {
            self.country = tmp
        }
        
        if let tmp: String = dict["country_cd"] as? String {
            self.countryCode = tmp
        }
        
        if let tmp: Int = dict["zip_cd"] as? Int {
            self.postalCode = "\(tmp)"
        } else {
            if let tmp: String = dict["zip_cd"] as? String {
                self.postalCode = tmp
            }
        }
        
        // Initialize Store Location
        if let latString: NSString = dict["latitude"] as? NSString {
            if let lngString: NSString = dict["longitude"] as? NSString {
                let lat: CLLocationDegrees = latString.doubleValue as CLLocationDegrees
                let lng: CLLocationDegrees = lngString.doubleValue as CLLocationDegrees
                self.location = CLLocation(latitude: lat, longitude: lng)
            }
        } else {
            self.location = CLLocation()
        }
        
        if let tmp: String = dict["category_nm"] as? String {
            self.category = tmp
        }
        
        if let tmp: Int = dict["map"] as? Int {
            self.mapped = tmp == 1
        } else {
            if let tmp: NSString = dict["map"] as? NSString {
                self.mapped = tmp.integerValue == 1
            }
        }
        
        if let tmp: Int = dict["pla_enabled_flg"] as? Int {
            self.searchable = tmp == 1
        } else {
            if let tmp: NSString = dict["pla_enabled_flg"] as? NSString {
                self.searchable = tmp.integerValue == 1
            }
        }
        
        if let tmp: String = dict["store_tobe_removed_flg"] as? String {
            self.willBeRemoved = tmp == "Y"
        }
        
        if let tmp: String = dict["flaggedForVisibility"] as? String {
            self.isVisible = tmp == "Y"
        }
        
        if let tmp: String = dict["store_logo_url"] as? String {
            self.logoURL = tmp
        }
        
        if let tmp: String = dict["store_map_url"] as? String {
            self.mapURL = tmp
        }
        
        if let tmp: String = dict["store_website_url"] as? String {
            self.websiteURL = tmp
        }
        
        //TODO: Handle standardized formatting, for now, just return what we get
        if let tmp: String = dict["phone_nbr"] as? String {
            self.phone = tmp
        }
        
        if let tmp: String = dict["hours"] as? String {
            self.hours = tmp
        }
        
        if let tmp: NSString = dict["distance"] as? NSString {
            self.distance = tmp.floatValue
        } else if let tmpF: Float = dict["distance"] as? Float {
            self.distance = tmpF
        }
        
        if let tmp: Int = ordinal {
            self.ordinal = tmp
        }
    }
    
    // MARK: NSCoding
    
    private struct SerializationKeys{
        static let id         = "id"
        static let name       = "name"
        static let vendorName = "vendorName"
        static let vendorId   = "vendorId"
        static let vendorVenueNumber   = "vendorVenueNumber"
        static let aisle411VenueNumber = "aisle411VenueNumber"
        static let address1    = "address1"
        static let address2    = "address2"
        static let city        = "city"
        static let region      = "region"
        static let regionCode  = "regionCode"
        static let country     = "country"
        static let countryCode = "countryCode"
        static let postalCode  = "postalCode"
        static let category    = "category"
        static let location    = "location"
        static let hours       = "hours"
        static let mapped      = "mapped"
        static let searchable  = "searchable"
        static let phone       = "phone"
        static let isVisible   = "isVisible"
        static let logoURL     = "logoURL"
        static let mapURL      = "mapURL"
        static let websiteURL  = "websiteURL"
        static let distance    = "distance"
        static let ordinal     = "ordinal"
        static let supportsVisitorLocation = "supportsVisitorLocation"
        static let locatorKitSettings      = "locatorKitSettings"
        static let normalizeLevelsForZones = "normalizeLevelsForZones"
        static let activities              = "activities"
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: SerializationKeys.id)
        aCoder.encode(self.name, forKey: SerializationKeys.name)
        aCoder.encode(self.vendorName, forKey: SerializationKeys.vendorName)
        aCoder.encode(self.vendorId, forKey: SerializationKeys.vendorId)
        aCoder.encode(self.vendorVenueNumber, forKey: SerializationKeys.vendorVenueNumber)
        aCoder.encode(self.aisle411VenueNumber, forKey: SerializationKeys.aisle411VenueNumber)
        aCoder.encode(self.address1, forKey: SerializationKeys.address1)
        aCoder.encode(self.address2, forKey: SerializationKeys.address2)
        aCoder.encode(self.city, forKey: SerializationKeys.city)
        aCoder.encode(self.region, forKey: SerializationKeys.region)
        aCoder.encode(self.regionCode, forKey: SerializationKeys.regionCode)
        aCoder.encode(self.country, forKey: SerializationKeys.country)
        aCoder.encode(self.countryCode, forKey: SerializationKeys.countryCode)
        aCoder.encode(self.postalCode, forKey: SerializationKeys.postalCode)
        aCoder.encode(self.category, forKey: SerializationKeys.category)
        aCoder.encode(self.location, forKey: SerializationKeys.location)
        aCoder.encode(self.hours, forKey: SerializationKeys.hours)
        aCoder.encode(self.mapped, forKey: SerializationKeys.mapped)
        aCoder.encode(self.searchable, forKey: SerializationKeys.searchable)
        aCoder.encode(self.phone, forKey: SerializationKeys.phone)
        aCoder.encode(self.isVisible, forKey: SerializationKeys.isVisible)
        aCoder.encode(self.logoURL, forKey: SerializationKeys.logoURL)
        aCoder.encode(self.mapURL, forKey: SerializationKeys.mapURL)
        aCoder.encode(self.websiteURL, forKey: SerializationKeys.websiteURL)
        aCoder.encode(self.distance, forKey: SerializationKeys.distance)
        aCoder.encode(self.ordinal, forKey: SerializationKeys.ordinal)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeInteger(forKey: SerializationKeys.id)
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as! String
        self.vendorName = aDecoder.decodeObject(forKey: SerializationKeys.vendorName) as! String
        self.vendorId = aDecoder.decodeInteger(forKey: SerializationKeys.vendorId)
        self.vendorVenueNumber = aDecoder.decodeInteger(forKey: SerializationKeys.vendorVenueNumber)
        self.aisle411VenueNumber = aDecoder.decodeInteger(forKey: SerializationKeys.aisle411VenueNumber)
        self.address1 = aDecoder.decodeObject(forKey: SerializationKeys.address1) as! String
        self.address2 = aDecoder.decodeObject(forKey: SerializationKeys.address2) as! String
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as! String
        self.region = aDecoder.decodeObject(forKey: SerializationKeys.region) as! String
        self.regionCode = aDecoder.decodeObject(forKey: SerializationKeys.regionCode) as! String
        self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as! String
        self.countryCode = aDecoder.decodeObject(forKey: SerializationKeys.countryCode) as! String
        self.postalCode = aDecoder.decodeObject(forKey: SerializationKeys.postalCode) as! String
        self.category = aDecoder.decodeObject(forKey: SerializationKeys.category) as! String
        self.location = aDecoder.decodeObject(forKey: SerializationKeys.location) as? CLLocation
        self.hours = aDecoder.decodeObject(forKey: SerializationKeys.hours) as! String
        self.mapped = aDecoder.decodeBool(forKey: SerializationKeys.mapped)
        self.searchable = aDecoder.decodeBool(forKey: SerializationKeys.searchable)
        self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as! String
        self.isVisible = aDecoder.decodeBool(forKey: SerializationKeys.isVisible)
        self.logoURL = aDecoder.decodeObject(forKey: SerializationKeys.logoURL) as! String
        self.mapURL = aDecoder.decodeObject(forKey: SerializationKeys.mapURL) as! String
        self.websiteURL = aDecoder.decodeObject(forKey: SerializationKeys.websiteURL) as! String
        self.distance = aDecoder.decodeFloat(forKey: SerializationKeys.distance)
        self.ordinal = aDecoder.decodeInteger(forKey: SerializationKeys.ordinal)
    }
}

/// Represents a user location triggered activity.
@objc public class IVKVenueActivity: IVKObject, NSCopying, NSCoding {
    /// Activity id.
    @objc
    public let id: Int
    
    /// Path to web page to display.
    @objc
    public let url: String
    
    /// Trigger type
    @objc
    public let triggerType: String
    
    /// Properties for the trigger.
    @objc
    public let triggerConfig: String
    
    /// Activity Type
    @objc
    public let activityType: String
    
    internal var valid: Bool = false
    
    ///Create a IVKVenueActivity with a dictionary.
    @objc
    public init?(dict: [String: AnyObject]) {
        guard let aId: Int = dict["activity_id"] as? Int else {
            return nil
        }
        guard let aURL: String = dict["url"] as? String else {
            return nil
        }
        guard let aType: String = dict["trigger_type"] as? String else {
            return nil
        }
        guard let aConfig: String = dict["trigger_config"] as? String else {
            return nil
        }
        if let aActivityType: String = dict["activity_type"] as? String {
            self.activityType = aActivityType
        } else {
            self.activityType = ""
        }
        self.id = aId
        self.url = aURL
        self.triggerType = aType
        self.triggerConfig = aConfig
        self.valid = true
        super.init()
    }
    
    private init(source: IVKVenueActivity) {
        self.id = source.id
        self.url = source.url.copy() as! String
        self.triggerType = source.triggerType.copy() as! String
        self.triggerConfig = source.triggerConfig.copy() as! String
        self.activityType = source.activityType.copy() as! String
        self.valid = source.valid
        super.init()
    }
    @objc public func copy(with: NSZone?) -> Any {
        return IVKVenueActivity(source: self) as Any
    }
    
    // MARK: NSCoding
    
    private struct SerializationKeys{
        static let id            = "id"
        static let url           = "url"
        static let triggerType   = "triggerType"
        static let triggerConfig = "triggerConfig"
        static let activityType  = "bufferPath"
        static let valid         = "valid"
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id,            forKey: SerializationKeys.id)
        aCoder.encode(self.url,           forKey: SerializationKeys.url)
        aCoder.encode(self.triggerType,   forKey: SerializationKeys.triggerType)
        aCoder.encode(self.triggerConfig, forKey: SerializationKeys.triggerConfig)
        aCoder.encode(self.activityType,  forKey: SerializationKeys.activityType)
        aCoder.encode(self.valid,         forKey: SerializationKeys.valid)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.id            = aDecoder.decodeInteger(forKey: SerializationKeys.id)
        self.url           = aDecoder.decodeObject(forKey: SerializationKeys.url)           as! String
        self.triggerType   = aDecoder.decodeObject(forKey: SerializationKeys.triggerType)   as! String
        self.triggerConfig = aDecoder.decodeObject(forKey: SerializationKeys.triggerConfig) as! String
        self.activityType  = aDecoder.decodeObject(forKey: SerializationKeys.activityType)  as! String
        self.valid         = aDecoder.decodeBool(forKey: SerializationKeys.valid)
    }
}
