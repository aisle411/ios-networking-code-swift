//
//  IVKVenueWithVenueItem.swift
//  VenueKit
//
//  Created by Fred Priese on 5/28/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
IVKVenue subclass that includes the best location in the venue for the search term using the fetchVenues() method with search term.
*/
@objc
public class IVKVenueWithVenueItem: IVKVenue {
    //MARK: - Properties
    /// Location in venue for the search result.
    @objc
    public let section: IVKSection
    
    /// Search term.
    @objc
    public let venueItemSearchTerm: String
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKVenueWithVenueItem {\n"
            desc += "\tsuper (IVKVenue): \(super.description)\n"
            desc += "\tsection: \(self.section)\n"
            desc += "\tvenueItemSearchTerm: \(self.venueItemSearchTerm)\n"
            desc += "}\n"
            return desc
        }
    }
    
    internal override init(dict: [NSString: AnyObject], ordinal: Int?) {
        var sect: String = ""
        var aisle: String = ""
        var mapLoc: Int = -1
        
        if let tmp: String = dict["sublocation"] as? String {
            sect = tmp
        }
        
        if let tmp: String = dict["location"] as? String {
            aisle = tmp
        }
        
        if let tmp: Int = dict["map_location"] as? Int {
            mapLoc = tmp
        } else {
            if let tmp: NSString = dict["map_location"] as? NSString {
                mapLoc = tmp.integerValue
            }
        }
        
        self.section = IVKSection(mapPointId: mapLoc, aisle: aisle, section: sect)
        
        if let tmp: String = dict["searchTerm"] as? String {
            self.venueItemSearchTerm = tmp
        } else {
            self.venueItemSearchTerm = ""
        }
        
        super.init(dict: dict, ordinal: ordinal)
    }
    
    //MARK: NSCoding
    
    private struct SerializationKeys{
        static let section             = "section"
        static let venueItemSearchTerm = "venueItemSearchTerm"
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.section = aDecoder.decodeObject(forKey: SerializationKeys.section) as! IVKSection
        self.venueItemSearchTerm = aDecoder.decodeObject(forKey: SerializationKeys.venueItemSearchTerm) as! String
        super.init(coder: aDecoder)
    }
    
    public override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(self.section, forKey: SerializationKeys.section)
        aCoder.encode(self.venueItemSearchTerm, forKey: SerializationKeys.venueItemSearchTerm)
    }

}
