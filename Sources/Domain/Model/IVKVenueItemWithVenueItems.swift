//
//  IVKVenueItemWithVenueItems.swift
//  VenueKit
//
//  Created by Fred Priese on 8/26/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/// Hierarchical Venue Item.
/// Experimental, should only ever be returned in Aisle411 demo instances and R&D builds.
@objc
public class IVKVenueItemWithVenueItems: IVKObject {
    //MARK: - Properties
    /// Item that is probably a Synonym or grouping name.
    @objc
    public let venueItem: IVKVenueItem
    
    /// Array of related child IVKVenueItem objects
    @objc
    public let venueItems: [IVKVenueItem]
    
    /// Description
    @objc public override var description: String {
        get {
            var desc: String = "IVKVenueItemWithVenueItems {\n"
            desc += "\tvenueItem: \(self.venueItem)\n"
            desc += "\tvenueItems: \(self.venueItems)\n"
            desc += "}\n"
            return desc
        }
    }
    
    internal init(parent: IVKVenueItem, children: [IVKVenueItem]) {
        self.venueItem = parent
        self.venueItems = children
        super.init()
    }
}
