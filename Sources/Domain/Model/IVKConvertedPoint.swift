//
//  IVKConvertedPoint.swift
//  VenueKit
//
//  Created by Minxin Guo on 11/3/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import UIKit

@objc public class IVKConvertedPoint: IVKObject {
    //MARK: - Properties
    private(set) public var x: Float = 0.0
    
    private(set) public var y: Float = 0.0
    
    private(set) public var floor: Int = 0
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKConvertedPoint {\n"
            desc += "\tx: \(self.x)\n"
            desc += "\ty: \(self.y)\n"
            desc += "\tfloor: \(self.floor)\n"
            desc += "}\n"
            return desc
        }
    }
    
    //MARK: - Initialization
    /// Create an empty instance.
    @objc
    public override init() {}
    
    /// Create an IVKCoordinate from lat and long values.
    @objc
    public init(x: Float, y: Float, floor: Int) {
        self.x = x
        self.y = y
        self.floor = floor
    }
}
