//
//  IVKVenueItem.swift
//  SKit
//
//  Created by Fred Priese on 4/7/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
A VenueItem including locations in a venue, if applicable.  Can be a Synonym, Typo/Suggestion og a Featured VenueItem as well.
*/
@objc
public class IVKVenueItem: IVKObject, NSCopying {
    //MARK: - Properties
    /// Type of venue item.
    public let venueItemType: IVKVenueItemType
    
    /// Name of venue item type.
    /// Is the raw value of the IVKVenueItemType.
    @objc
    public var venueItemTypeName: String {
        get {
            return self.venueItemType.rawValue
        }
    }
    
    /// What this item is called.
    @objc
    private(set) public var name: String = ""
    
    /** Convenience property for name or synonym.
     This should always return a value for display purposes.
     Will return the first value it finds in:
     1. .name
     2. The first section name in the sections array.
     3. .synonym
     4. .venueItemSearchTerm
     */
    @objc
    public var normalizedName: String {
        if name.characters.count > 0 {
            return self.name
        } else {
            if self.sectionName.characters.count > 0 {
                return self.sectionName
            } else {
                if self.synonym.characters.count > 0 {
                    return self.synonym
                } else {
                    return self.venueItemSearchTerm
                }
            }
        }
    }

    /// What you passed into the search method.
    @objc
    private(set) public var venueItemSearchTerm: String = ""

    /// Sections where this item can be found.
    @objc
    private(set) public var sections: [IVKSection] = [IVKSection]()
    
    /// Selected section for this instance of the venue item.
    internal var selectedSection: IVKSection? = nil
    
    @objc
    private(set) public var sectionName: String = ""
    
    /// Besides being a great purveyor of video games, in this case it is just the unique identifier of the item.
    @objc
    private(set) public var id: Int = -1
    
    /// The brand.  No irons needed.
    @objc
    private(set) public var brand: String = ""
    
    /// UPC of the item, or an empty String.
    @objc
    private(set) public var upc: String = ""
    
    /// Description from the cloud.
    @objc
    private(set) public var venueItemDescription: String = ""
    
    /// Path to a coupon.
    @objc
    private(set) public var couponURL: String = ""
    
    /// The opposite of antonym.
    @objc
    private(set) public var synonym: String = ""
    
    /// Id for Synonym.
    @objc
    private(set) public var synonymId: Int = -1
    
    /// Ordinal position of the item in search results.
    @objc
    private(set) public var ordinal: Int = 0
    
    /// Path to image for item.
    @objc
    private(set) public var itemImageURL: String = ""
    
    /// Price for nonpriceless items.
    @objc
    private(set) public var price: Double = 0.0
    
    /// Discounted price for nonpriceless items.
    @objc
    private(set) public var discountedPrice: Double = 0.0
    
    /// Can be acted upon in some way not provided in IVK.
    @objc
    private(set) public var isActionable: Bool = false
    
    private let source: [String: AnyObject]
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKVenueItem {\n"
            desc += "\tvenueItemType: \(self.venueItemType.rawValue)\n"
            desc += "\tid: \(self.id)\n"
            desc += "\tnormalizedName: \(self.normalizedName)\n"
            desc += "\tname: \(self.name)\n"
            desc += "\tvenueItemSearchTerm: \(self.venueItemSearchTerm)\n"
            desc += "\tsections: \(self.sections)\n"
            desc += "\tbrand: \(self.brand)\n"
            desc += "\tupc: \(self.upc)\n"
            desc += "\tvenueItemDescription: \(self.venueItemDescription)\n"
            desc += "\tcouponURL: \(self.couponURL)\n"
            desc += "\tsynonym: \(self.synonym)\n"
            desc += "\tsynonymId: \(self.synonymId)\n"
            desc += "\tordinal: \(self.ordinal)\n"
            desc += "\titemImageURL: \(self.itemImageURL)\n"
            desc += "\tprice: \(self.price)\n"
            desc += "\tdiscountedPrice: \(self.discountedPrice)\n"
            desc += "\tisActionable: \(self.isActionable)\n"
            desc += "}\n"
            return desc
        }
    }
    
    internal init(dict: [String: AnyObject], ordinal: Int?, mapPointId: Int? = nil) {
        self.source = dict
        if let _:String = dict["item_id"] as? String {
            if let _: String = dict["coupon_url"] as? String {
                self.venueItemType = IVKVenueItemType.featuredVenueItem
            } else {
                self.venueItemType = IVKVenueItemType.venueItem
            }
        } else {
            if let synonymId: Int = dict["synonym_id"] as? Int  {
                self.synonymId = synonymId
                self.venueItemType = IVKVenueItemType.synonym
            } else {
                self.venueItemType = IVKVenueItemType.typoSuggestion
            }
        }
        
        if let tmp: Int = dict["item_id"] as? Int {
            self.id = tmp
        } else {
            if let tmp: NSString = dict["item_id"] as? NSString {
                self.id = tmp.integerValue
            }
        }
        
        if let tmp: String = dict["item_nm"] as? String {
            self.name = tmp
        }
        
        if let tmp: String = dict["name"] as? String {
            self.venueItemSearchTerm = tmp
        }
        
        if let tmp: String = dict["synonym_nm"] as? String {
            self.synonym = tmp
        }
        
        // set IVKVenueItem properties
        if let tmp: String = dict["brand_nm"] as? String {
            self.brand = tmp
        }
        
        if let tmp: String = dict["upc"] as? String {
            self.upc = tmp
        }
        
        if let tmp: String = dict["item_dsc"] as? String {
            self.venueItemDescription = tmp
        }
        
        if let tmp: String = dict["coupon_url"] as? String {
            self.couponURL = tmp
        }
        
        if let tmp: Int = ordinal {
            self.ordinal = tmp
        }
        
        if let sections: [[String: AnyObject]] = dict["sections"] as? [[String: AnyObject]] {
            for section in sections {
                var mapLocId: Int = -1
                var aisle: String = ""
                var sect: String = ""

                if let tmp: Int = section["map_location_id"] as? Int {
                    mapLocId = tmp
                } else {
                    if let tmp: NSString = section["map_location_id"] as? NSString {
                        mapLocId = tmp.integerValue
                    }
                }

                if let tmp: String = section["aisle"] as? String {
                    aisle = tmp
                }
                
                if let tmp: String = section["section"] as? String {
                    sect = tmp
                }
                
                if let _ = mapPointId {
                    if mapLocId == mapPointId! {
                        self.sectionName = sect
                        let ivkSection: IVKSection = IVKSection(mapPointId: mapLocId, aisle: aisle, section: sect)
                        self.sections.append(ivkSection)
                    }
                } else {
                    let ivkSection: IVKSection = IVKSection(mapPointId: mapLocId, aisle: aisle, section: sect)
                    self.sections.append(ivkSection)
                }
            }
        }
        
        if let _ = dict["map_location_id"] {
            var mapLocId: Int = -1
            var aisle: String = ""
            var sect: String = ""
            
            if let tmp: Int = dict["map_location_id"] as? Int {
                mapLocId = tmp
            } else {
                if let tmp: NSString = dict["map_location_id"] as? NSString {
                    mapLocId = tmp.integerValue
                }
            }
            
            if let tmp: String = dict["aisle"] as? String {
                aisle = tmp
            }
            
            if let tmp: String = dict["section"] as? String {
                sect = tmp
            }
            
            let ivkSection: IVKSection = IVKSection(mapPointId: mapLocId, aisle: aisle, section: sect)
            self.sections.append(ivkSection)
        }
        
        if let tmp: String = dict["item_image_url"] as? String {
            self.itemImageURL = tmp
        }
        
        
        if let tmp: Double = dict["price"] as? Double {
            self.price = tmp
        } else {
            if let tmp: NSString = dict["price"] as? NSString {
                self.price = tmp.doubleValue
            }
        }

        if let tmp: Double = dict["discounted_price"] as? Double {
            self.discountedPrice = tmp
        } else {
            if let tmp: NSString = dict["discounted_price"] as? NSString {
                self.discountedPrice = tmp.doubleValue
            }
        }
        
        if let tmp: Bool = dict["is_actionable"] as? Bool {
            self.isActionable = tmp
        }
        
        //ignoring these for now...
//        "aisle411_item_location_cd":"1",
//        "item_sub_location_dsc":null,
//        "user_sourced_flg":"N",
//        "seasonal_flg":"N",
//        "sku":null,
//        "item_type_cd":"I",
//        "item_common_nm":null,
//        "brand_id":6257,
//        "brand_nm":"Favorit",
//        "include_in_grammar_flg":"Y",
//        "offer_summary":null,
//        "offer_dsc":null,
//        "offer_image_url":null
    }
    
    @objc
    public func copy(with: NSZone?) -> Any {
        let copy: IVKVenueItem = IVKVenueItem(dict: self.source, ordinal: self.ordinal)
        return copy as AnyObject
    }
}
