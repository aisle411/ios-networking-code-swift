//
//  IVKSection.swift
//  VenueKit
//
//  Created by Fred Priese on 5/28/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
IVKSection represents a location within a IVKVenue.
*/
@objc public class IVKSection: IVKObject, NSCoding {
    
    
    //MARK: - Properties
    /// Unique identity for a known location in a map.
    @objc
    private(set) public var mapPointId: Int = -1
    
    /// The name of the aisle or larger area of the section.
    @objc
    private(set) public var aisle: String = ""
    
    /// The name of the section of the larger area.
    @objc
    private(set) public var section: String = ""
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKSection {\n"
            desc += "\tmapPointId: \(self.mapPointId)\n"
            desc += "\taisle: \(self.aisle)\n"
            desc += "\tsection: \(self.section)\n"
            desc += "}\n"
            return desc
        }
    }
    
    internal init(mapPointId: Int, aisle: String, section: String) {
        self.mapPointId = mapPointId
        self.aisle = aisle.replacingOccurrences(of: "Store", with: "", options: .literal, range: nil)
        self.section = section
    }
    
    //MARK: NSCoding
    private struct SerializationKeys{
        static let mapPointIdKey = "name"
        static let aisleKey      = "aisle"
        static let sectionKey    = "points"
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.mapPointId = aDecoder.decodeInteger(forKey: SerializationKeys.mapPointIdKey)
        self.aisle      = aDecoder.decodeObject(forKey: SerializationKeys.aisleKey)       as! String
        self.section    = aDecoder.decodeObject(forKey: SerializationKeys.sectionKey)     as! String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.mapPointId, forKey: SerializationKeys.mapPointIdKey)
        aCoder.encode(self.aisle,      forKey: SerializationKeys.aisleKey)
        aCoder.encode(self.section,    forKey: SerializationKeys.sectionKey)
    }
}

/// Returns true if the mapPointId values are the same.
public func == (s1: IVKSection, s2: IVKSection) -> Bool {
    return s1.mapPointId == s2.mapPointId
}
