//
//  IVKAccount.swift
//  IVKLive
//
//  Created by Dante Cannarozzi on 8/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/**
 IVKAccount represents a user account in the Aisle411 system.  Use `(IVKApplication).signin(_:, _:, _:)` to acquire a valid instance of this with a valid email and password.  Can be handy to use as a container for venues if an app should have need of using multiple venues.  There is no benefit to doing this though.
 */
@objc public class IVKAccount: IVKObject {
    //MARK: - Properties
    /// Unique system account identity.
    @objc
    private(set) public var accountId: Int = -1

    /// Array of IVKVenue instances the account has access to viewing.  Can be overridden by calling `initVenues()`.
    @objc
    private(set) public var venues: [IVKVenue] = []
    
    /// Objective-C NSArray of IVKVenue instances the account has access to viewing.  Can be overridden by calling `initVenues()`.
    @objc
    private(set) public var arrayOfVenues: NSArray = []
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKAccount {\n"
            desc += "\taccountId: \(self.accountId)\n"
            desc += "\tvenues: \(self.venues)\n"
            desc += "}\n"
            return desc
        }
    }
    
    //MARK: - Methods
    /// Override method for replacing or setting the venues.  Can be useful as a container for venue objects.
    @objc
    public func initVenues(venues: [IVKVenue]) {
        self.venues = venues
    }
    
    //MARK: - Initialization
    /// Override initializer that returns an empty IVKAccount instance.  You can add venues to this using `initVenues(_:)`.
    @objc
    public override init() {}
    
    internal init(dict: [NSString:AnyObject], accountId: Int? = nil) {
        if let id = accountId {
            self.accountId = id
        }
        if let tmp: [[String: AnyObject]] = dict["venues"] as? [[String: AnyObject]] {
            for v in tmp {
                self.venues.append(IVKVenue(dict: v as [NSString : AnyObject], ordinal:0))
            }
        }
    }
}
