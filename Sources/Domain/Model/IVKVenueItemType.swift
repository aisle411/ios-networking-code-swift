//
//  IVKVenueItemType.swift
//  VenueKit
//
//  Created by Fred Priese on 8/26/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/**
 Types of items returned from search.
 - VenueItem: An actual item (a branded product of some size, a point of interest, etc.).
 - Synonym: A result meaningful to this venue that may represent one or more VenueItems.
 - TypoSuggestion: A result suggesting an alternative term for the search based on an assumption of a typo in the original search term.
 - FeaturedVenueItem: Not JUST an actual item, but a FEATURED actual item.
 */
public enum IVKVenueItemType: String {
    ///An actual item (a branded product of some size, a point of interest, etc.).
    case venueItem = "VenueItem"
    ///A result meaningful to this venue that may represent one or more VenueItems.
    case synonym = "Synonym"
    ///A result suggesting an alternative term for the search based on an assumption of a typo in the original search term.
    case typoSuggestion = "TypoSuggestion"
    ///Not JUST an actual item, but a FEATURED actual item.
    case featuredVenueItem = "FeaturedVenueItem"
}
