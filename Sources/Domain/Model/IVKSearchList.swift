//
//  IVKSearchList.swift
//  VenueKit
//
//  Created by Fred Priese on 10/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/// Struct for passing a list of IVKSearchItems to IVMKMapView.search(items:).  Aka: A shopping list struct.
@objc public class IVKSearchList: IVKObject, NSCopying {
    /// Name of the list, not required, is not used.
    @objc
    public var name: String = ""
    /// Array of IVKSearchItem instances.
    @objc
    public var items: [IVKSearchItem] = []
    /// Dictionary in the format required for executing the search API.  Also can be used to pass into IVMKMapView.search(dict:) and IVKApplication.search(dict:)
    ///Dictionary representation consistent with the legacy shopping list search.
    @objc
    public var dictionary: [String: AnyObject]? {
        get {
            var dict: [String: AnyObject] = [:]
            dict["name"] = self.name as AnyObject
            var items: [[String: AnyObject]] = []
            for item in self.items {
                if let dict = item.dictionary {
                    items.append(dict)
                }
            }
            if items.count > 0 {
                dict["items"] = items as AnyObject
                return dict
            }
            return nil
        }
    }
    
    /// Init with an array of search terms.
    @objc
    public init(terms: [String]) {
        for term in terms {
            self.items.append(IVKSearchItem(term: term))
        }
    }
    
    /// Init with an array of IVKSearchItems.
    @objc
    public init(items: [IVKSearchItem]) {
        for item in items {
            self.items.append(item)
        }
    }
    
    /// Append a search item with passed term.
    @objc
    public func append(term: String) {
        self.items.append(IVKSearchItem(term: term))
    }
    /// Append a search item with passed upc.
    @objc
    public func append(upc: String) {
        self.items.append(IVKSearchItem(upc: upc))
    }
    /// Append a search item with passed item.
    @objc
    public func append(item: IVKSearchItem) {
        self.items.append(item)
    }
    
    @objc
    public func copy(with zone: NSZone? = nil) -> Any {
        var items: [IVKSearchItem] = []
        for item in self.items {
            items.append(item.copy() as! IVKSearchItem)
        }
        let copy = IVKSearchList(items: items)
        copy.name = self.name.copy() as! String
        return copy as Any
    }
}
