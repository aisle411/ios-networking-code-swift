//
//  IVKRequestDiagnostic.swift
//  VenueKit
//
//  Created by Fred Priese on 7/20/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
Object containing diagnostic information about an IVKRequest API request such as timings, some headers and the response.
*/
internal class IVKRequestDiagnostic: IVKObject {
    /// URL object used for the API request.
    internal let url: URL!
    /// When the request began preparing.
    internal let start: Date!
    private let started: Bool = true
    private var ended: Bool = false
    private var startedNetwork: Bool = false
    private var endedNetwork: Bool = false
    
    private var _startNetwork: Date?
    /// When the asynchronous request was initiated.
    internal var startNetwork: Date? {
        get {
            return self._startNetwork
        }
    }
    
    private var _endNetwork: Date?
    /// When the asynchronous request responded.
    internal var endNetwork: Date? {
        get {
            return self._endNetwork
        }
    }
    
    private var _end: Date?
    /// When the post response processing was complete.
    internal var end: Date? {
        get {
            return self._end
        }
    }
    
    /// Total time, in seconds, it took for the request to be processed.
    internal var requestDuration: TimeInterval? {
        get {
            if self.started && self.ended {
                return self._end!.timeIntervalSince(self.start)
            }
            return nil
        }
    }

    /// Total time, in seconds, it took for the asynchronous netwrok request to be processed.
    internal var requestNetworkDuration: TimeInterval? {
        get {
            if self.startedNetwork && self.endedNetwork {
                return self._endNetwork!.timeIntervalSince(self.startNetwork!)
            }
            return nil
        }
    }
    
    private var _bytes: Int = 0
    /// Size, in bytes of the response.
    internal var bytesForResponse: Int {
        get {
            return _bytes
        }
    }
    
    private var _status: Int?
    /// HTTP status code of the response, (200, 404, etc.)
    internal var httpStatus: Int? {
        get {
            return self._status
        }
    }
    
    private var _lastModifiedHeader: String?
    /// Header value for Last Modified Date
    internal var lastModifiedHeader: String? {
        return self._lastModifiedHeader
    }
    /// Last Modified Date.
    internal var lastModifiedDate: Date? {
        get {
            if let lMH: String = self._lastModifiedHeader {
                babelfish.verbose("Last Modified Date of request: \(lMH)")
                let formatter: DateFormatter = DateFormatter()
                formatter.dateFormat = "EEE, dd MMM yyyy kk:mm:ss zzz"
                formatter.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
                if let lmDate: Date = formatter.date(from: lMH) {
                    return lmDate
                }
            }
            return nil
        }
    }
    
    internal init(url: URL, startTime: Date) {
        self.url = url
        self.start = startTime
    }

    internal func beginNetwork() {
        self._startNetwork = Date()
        self.startedNetwork = true
    }
    
    internal func processResponse(response: HTTPURLResponse, byteSize: Int) {
        self._endNetwork = Date()
        self.endedNetwork = true
        
        self._status = response.statusCode
        self._bytes = byteSize
        
        if self._status! > 0 {
            if let lastModified: String = response.allHeaderFields["Last-Modified"] as? String {
                self._lastModifiedHeader = lastModified
            }
        }
    }
    
    internal func endDiagnostic() {
        self._end = Date()
        self.ended = true
    }
    
//    deinit {
//        babelfish.severe("IVKRequestDiagnostic is being released.")
//    }
}
