//
//  IVKAuthParam.swift
//  VenueKit
//
//  Created by Fred Priese on 10/10/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKAuthParam: IVKObject {
    internal let key: String
    internal var escapedKey: String {
        get {
            if let e = self.key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return e
            } else {
                return ""
            }
        }
    }
    
    private let _array: [[String:Any]]?
    internal var array: [[String:Any]]? {
        get {
            return self._array
        }
    }
    
    private let _dictionary: [String: Any]?
    internal var dictionary: [String: Any]? {
        get {
            return self._dictionary
        }
    }
    
    internal let value: String
    internal var escapedValue: String {
        get {
            if let e = self.value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return e
            } else {
                return ""
            }
        }
    }
    internal func queryString(encode: Bool) -> String {
        if encode {
            return "\(self.escapedKey)=\(self.escapedValue)"
        } else {
            return "\(self.key)=\(self.value)"
        }
    }
    
    internal init(key: String, value: String) {
        self.key = key
        self.value = value
        self._dictionary = nil
        self._array = nil
        super.init()
    }
    
    internal init(key: String, dictionary: [String: Any]) {
        self.key = key
        self.value = "\(dictionary)"
        self._dictionary = dictionary
        self._array = nil
        super.init()
    }
    
    
    internal init(key: String, array: [[String:Any]]) {
        self.key = key
        self.value = "\(array)"
        self._dictionary = nil
        self._array = array
        super.init()
    }
}
