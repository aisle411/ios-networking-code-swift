//
//  IVKObject.swift
//  VenueKit
//
//  Created by Fred Priese on 8/17/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
`IVKObject` is the base class for most VenueKit classes.  Some classes inherit from various Apple classes.
*/
@objc
public class IVKObject: NSObject {
    //MARK: - Properties
    /// Human readable formatted outut of this object.  If you subclass IVKObject, you should override this.
    @objc
    override public var description: String {
        get {
            // TO DO : add valid description
            let desc: String = "The override description for this IVKObject sub-class has not been implemented.  Please note the sub-class name and report as a bug to Aisle411."
            let prefix: String = "VenueKit.IVKObject.\(super.description)"
            return prefix + desc
        }
    }

    //MARK: - Methods
    /// Makes directory if it does not exist.  Returns true if dir is in existence when done, false if a throw happens.
    /// For convenience.
    @discardableResult @objc
    public func makeDir(url: URL) -> Bool {
        let finder: FileManager = FileManager.default
        if !finder.fileExists(atPath: url.path) {
            do {
                try finder.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
                return true
            } catch {
                babelfish.info("makeDir(\(url)) threw up on createDirectoryAtURL.  Clean up in Aisle411.")
                return false
            }
        }
        return true
    }
   
    /// Runs passed block on main thread.  If running on main thread, then block is executed on the same thread.
    /// For convenience.
    @objc
    public func runOnMainThread(_ block: @escaping (() -> Void)) {
        IVKObject.runOnMainThread(block)
    }
    
    /// Runs passed block asynchronously in a new queue called `asyncer`.
    /// For convenience.
    @objc
    public func runAsync(_ block: @escaping (() -> Void)) {
        let q = DispatchQueue(label: "asyncer", qos: .default, attributes: DispatchQueue.Attributes(), autoreleaseFrequency: .inherit, target: nil)
        q.async(execute: block)
    }
    
    internal class func boolForValue(value: AnyObject) -> Bool {
        if value is Bool {
            return (value as! Bool)
        } else if value is String {
            let vString: String = value as! String
            return (vString.lowercased() == "true")
        }
        return false
    }
    
    internal class func floatForValue(value: AnyObject) -> Float {
        if let val: NSNumber = value as? NSNumber {
            return val.floatValue
        }
        if let val: String = value as? String {
            if let floater: Float = Float(val) {
                return floater
            }
        }
        return 0.0
    }
    
    internal static func runOnMainThread(_ block: @escaping (() -> Void)) {
        if Thread.isMainThread {
            block()
        } else {
            DispatchQueue.main.async(execute: block)
        }
    }
}
