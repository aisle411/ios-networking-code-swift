//
//  IVKSuggestions.swift
//  VenueKit
//
//  Created by Fred Priese on 9/19/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKSuggestion: IVKObject {
    let name: String
    let categoryId: Int
    let subCategoryId: Int
    let id: Int
        
    internal init?(dict: [String: AnyObject]) {
        if let name: String = dict["name"] as? String {
            self.name = name
        } else {
            return nil
        }
        
        if let categoryId: Int = dict["category"] as? Int {
            self.categoryId = categoryId
        } else {
            self.categoryId = -1
        }

        if let subCategoryId: Int = dict["subcategory"] as? Int {
            self.subCategoryId = subCategoryId
        } else {
            self.subCategoryId = -1
        }
        
        if let id: Int = dict["id"] as? Int {
            self.id = id
        } else {
            self.id = -1
        }
    }
}

internal class IVKSuggestions: IVKObject {
    private let suggestions: [IVKSuggestion]
    
    /// Will return a list of suggested search terms that begin with the trimmed text passed.  If includeContained is passed as true, then the results will include additional suggested search terms that contain the trimmed text appended to the end of the suggested search terms that begin with the trimmed text.e
    internal func suggestionsFor(text: String, includeContained: Bool = false) -> [String] {
        func uniq<T: Hashable>(lst: [T]) -> [T] {
            var seen = Set<T>(minimumCapacity: lst.count)
            return lst.filter { x in
                let unseen = !seen.contains(x)
                seen.insert(x)
                return unseen
            }
        }
        
        let term: String = text.trim()
        if term.length == 0 {
            return []
        }
        let bwPredicate: NSPredicate = NSPredicate(format: "name BEGINSWITH[cd] %@", term)
//        let bwPredicate: NSPredicate = NSPredicate { (sugg: AnyObject, bindings: [String: AnyObject]?) in
//            guard let suggestion: IVKSuggestion = sugg as? IVKSuggestion else {
//                return false
//            }
//            return suggestion.name.indexOf() != nil
//            
//            
//            return firstName != nil || lastName != nil
//        }
        let source: NSArray = self.suggestions as NSArray
        let rArray: NSArray = source.filtered(using: bwPredicate) as NSArray
        
        var results: [String] = []
        if let _results: [IVKSuggestion] = rArray as? [IVKSuggestion] {
            for s in _results {
                results.append(s.name)
            }
            results.sort(by: <)
        }
        
        if includeContained {
            let cPredicate: NSPredicate = NSPredicate(format: "name CONTAINS[cd] '\(term)'")
            let sArray: NSArray = source.filtered(using: cPredicate) as NSArray
            
            var supplemental: [String] = []
            if let _supplemental: [IVKSuggestion] = sArray as? [IVKSuggestion] {
                for s in _supplemental {
                    supplemental.append(s.name)
                }
                supplemental.sort(by: <)
            }
            
            if supplemental.count > 0 {
                results = results + supplemental
                results = uniq(lst: results)
            }
        }
        
        return results
    }
    
    internal init(array: [[String: AnyObject]]) {
        var suggs: [IVKSuggestion] = []
        for obj in array {
            if let sugg: IVKSuggestion = IVKSuggestion(dict: obj) {
                suggs.append(sugg)
            }
        }

        self.suggestions = suggs
    }
}
