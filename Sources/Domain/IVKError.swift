//
//  IVKError.swift
//  VenueKit
//
//  Created by Fred Priese on 6/1/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation.CLError

/**
Defined Error types for VenueKit.  Integer values are meant for better Obj-C support and not to be terribly meaningful.
*/
public enum IVKErrorType: Int {
    //MARK: Error Keys and Numbers
    /// Success, no error.
    case success = 0
    /// Authentication failed.
    case authenticationFailed = 1
    /// Response was empty.
    case emptyResponse = 2
    /// An HTTPRequestError happened.
    case httpRequestError = 3
    /// Invalid parameter
    case invalidParameter = 4
    /// Invalid parameter
    case locationNotAvailable = 5
    /// Brigadier General Error
    case generalError = 1337
    
    //MARK: Error Messages
    /// Error message.
    public func message() -> String {
        switch self {
        case .success:
            return "Sought, these bugs are not."
        case .authenticationFailed:
            return "The Aisle411 VenueKit SDK Authentication credentials are invalid."
        case .emptyResponse:
            return "The HTTP response was empty"
        case .httpRequestError:
            return "The HTTP request errored."
        case .invalidParameter:
            return "Invalid parameter."
        case .locationNotAvailable:
            return "Location isn't available."
        case .generalError:
            //A suffusion of Yellow.
            return "General Error.  (Typically, this is output from a framework other than VenueKit.)"
//        default:
//            return String(self.rawValue)
        }
    }
}

/**
`IVKError` is the standard error object for VenueKit.
*/
@objc
public class IVKError: IVKObject {
    private let _errorType: IVKErrorType
    private let _adhocNumber: Int
    private let _adhocMessage: String
    public let additionalInfo: String
    
    //MARK: - Properties
    /// Error number that corresponds to the IVKErrorType OR provided by external error such as NSError.
    @objc
    public var number: Int {
        get {
            if (self._errorType == IVKErrorType.generalError && self._adhocNumber != 0) ||
                self._errorType == IVKErrorType.locationNotAvailable {
                return self._adhocNumber
            } else {
                return self._errorType.rawValue
            }
        }
    }
    
    /// Error message that corresponds to the IVKErrorType OR provided by external error such as NSError.
    @objc
    public var message: String {
        get {
            if self._errorType == IVKErrorType.generalError && self._adhocMessage != "" {
                return self._adhocMessage
            } else {
                return self._errorType.message()
            }
        }
    }
    
    /// Specific IVKErrorType for this object.
    public var type: IVKErrorType {
        get {
            return self._errorType
        }
    }
    /// The IVKErrorType's raw value.
    @objc
    public var typeId: Int {
        get {
            return self.type.rawValue
        }
    }
    /// The IVKErrorType's error message.
    @objc
    public var typeName: String {
        get {
            return self.type.message()
        }
    }
    
    /// Description
    @objc
    public override var description: String {
        get {
            var desc: String = "IVKError {\n"
            desc += "\ttypeId: \(self.typeId)\n"
            desc += "\ttypeName: \(self.typeName)\n"
            desc += "\tadhoc number: \(self._adhocNumber) (Default is 0)\n"
            desc += "\tadhoc message: \(self._adhocMessage) (Default is empty string)\n"
            desc += "\tadditional information: \(self.additionalInfo) (Default is empty string)\n"
            desc += "}\n"
            return desc
        }
    }
    
    /// Create an IVKError with an NSError.
    @objc
    public convenience init(nsError: NSError) {
        if nsError.domain == CLError._nsErrorDomain {
            self.init(errorType: IVKErrorType.locationNotAvailable, errorNumber: nsError.code)
        } else {
            self.init(errorNumber: nsError.code , errorMessage: nsError.localizedDescription)
        }
    }
    
    internal init(errorType: IVKErrorType, additionalInfo: String = "", errorNumber: Int = 0) {
        self._errorType = errorType
        self._adhocMessage = ""
        self._adhocNumber = errorNumber
        self.additionalInfo = additionalInfo
    }
    
    internal init(errorNumber: Int, errorMessage: String, additionalInfo: String = "") {
        var _errorType: IVKErrorType = .generalError
        // Handle situation where IVKErrorType is being passed in.  Should be infrequent.
        if let type: IVKErrorType = IVKErrorType(rawValue: errorNumber) {
            if errorMessage == type.message() {
                _errorType = type
            }
        }
        self._errorType = _errorType
        self._adhocNumber = errorNumber
        self._adhocMessage = errorMessage
        self.additionalInfo = additionalInfo
        super.init()
    }
}
