//
//  IVKAuthParams.swift
//  VenueKit
//
//  Created by Fred Priese on 10/10/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKAuthParams: IVKObject {
    var params: [IVKAuthParam] = []
    
    internal override init() {
        super.init()
    }
    
    private init(array: [IVKAuthParam]) {
        super.init()
        self.params = array
    }
    
    internal func count() -> Int {
        return self.params.count
    }
    
    internal func sorted() -> [IVKAuthParam] {
        let sorted = self.params.sorted { (p1, p2) -> Bool in
            return p1.key < p2.key
        }
        return sorted
    }
    
    internal func queryString(encode: Bool = false) -> String {
        let sorted: [IVKAuthParam] = self.sorted()
        var amp: String = "?"
        var query: String = ""
        for authParam in sorted {
            query += "\(amp)\(authParam.queryString(encode: encode))"
            amp = "&"
        }
        return query
    }
    
    internal func dataForHTTPPost() -> Data? {
        let sorted: [IVKAuthParam] = self.sorted()
        let opt: JSONSerialization.WritingOptions = .prettyPrinted
        var dict: [String: Any] = [:]
        for authParam in sorted {
            if let pDict = authParam.dictionary {
                dict[authParam.key] = pDict as Any
            } else if let pArray = authParam.array {
                dict[authParam.key] = pArray as Any
            } else {
                dict[authParam.key] = authParam.value
            }
        }
        do {
            let body: Data = try JSONSerialization.data(withJSONObject: dict, options: opt)
            return body
        } catch {
            return nil
        }
    }
    
    internal func authStringForHTTPPost() -> String {
        if let bodyForAuth: Data = self.dataForHTTPPost() {
            let authBody: String = NSString(data: bodyForAuth, encoding: String.Encoding.utf8.rawValue) as! String
            return authBody
        }
        return ""
    }
}
