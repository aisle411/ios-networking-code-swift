
import Foundation
import CoreLocation

public class AisleServer: NSObject {
    
    @objc
    public static let shared = AisleServer()
    
    @objc
    public static let kIVKReadyNotificationName = "com.aisle411.venuekit.ready"
    
    //MARK: - Internal Static Constants
    internal static let kIVKAnonymousDeviceIdentityKey = "IVKAnonymousDeviceIdentityKey"
    internal static let kVenueKitId = "com.aisle411.VenueKit"
    internal static let kDefaultsGroupKey = "\(AisleServer.kVenueKitId).application.group"
    internal static let kApplicationNameKey = "\(AisleServer.kVenueKitId).application.name"
    internal static let kApplicationVersionKey = "\(AisleServer.kVenueKitId).application.version"
    
    private let dataCache: IVKDataCache
    internal var suggestions: IVKSuggestions?
    
    internal let rootCachePath: URL
    private let rootMapsCachePath: URL
    private let searchSuggestionDBPath: URL   // SQLite DB
    private let searchSuggestionPath: URL     // JSON based text file
    
    private let defaultAPIHost: String = "aisle411.ws"
    private let defaultAPIVersion: Int = 3
    
    internal let credentialed: Bool
    
    private var locationService: LocationService!
        
    //MARK: - Private Constants
    private static let kDefaultModifiedDate = Date(timeIntervalSince1970: 0.0)
    
    private let _credentials: [String: AnyObject]
    private var credentials: [String: AnyObject] {
        get {
            if self._apiHost != self.apiHost {
                var creds = self._credentials
                creds["host"] = self.apiHost as AnyObject?
                return creds
            }
            return self._credentials
        }
    }
    private let _apiHost: String
    internal var apiHost: String {
        get {
            if let _ = self.customHost {
                return self.customHost!
            } else {
                return self._apiHost
            }
        }
    }
    
    // for reuqest
    internal let partnerID: Int
    internal let partnerSecret: String
    internal let apiBase: String = "webservices"
    internal let api: String
    internal var useSSL: Bool = true
    private var customHost: String?
    
    
    // MARK:- init
    
    private convenience init?(useBundle: Bool = true) {
        let bundle = Bundle.main
        let credentials: NSDictionary
        if let credentialsPath = bundle.path(forResource: "com.aisle411.venuekit", ofType: "plist") {
            credentials = NSDictionary(contentsOfFile: credentialsPath)!
        } else {
            return nil
        }
        
        self.init(credentials: credentials as! [String : AnyObject])
    }
    
    // This init will use the credentials dictionary provided to authenticate and authorize Aisle411 web service access.
    private init?(credentials: [String: AnyObject]) {
        func sharpenTheAxe(rootCachePath: URL) {
            func levelForString(iLogLevel: String) -> XCGLogger.Level {
                switch iLogLevel.lowercased() {
                case "meta" :
                    return XCGLogger.Level.meta
                case "verbose" :
                    return XCGLogger.Level.verbose
                case "debug" :
                    return XCGLogger.Level.debug
                case "info" :
                    return XCGLogger.Level.info
                case "warning" :
                    return XCGLogger.Level.warning
                case "error" :
                    return XCGLogger.Level.error
                case "severe" :
                    return XCGLogger.Level.severe
                default :
                    return XCGLogger.Level.none
                }
            }
            var xcLogLevel: XCGLogger.Level = XCGLogger.Level.none
            var lfLogLevel: XCGLogger.Level? = nil
            var lfPath: String? = nil
            
            #if LOGGABLE
                let bundle = Bundle.main
                let info = bundle.infoDictionary!
                // What log level is defined in info.plist?
                if let iLogLevel: String = info["com.aisle411.ivk.loglevel.xcode"] as? String {
                    xcLogLevel = levelForString(iLogLevel: iLogLevel)
                }
                // Is app by Aisle411?
                if bundle.bundleIdentifier!.indexOf("com.aisle411.") != nil {
                    // Yes! Then we'll even check to see if we should log to file.
                    if let iLogLevel: String = info["com.aisle411.ivk.loglevel.logfile"] as? String {
                        lfLogLevel = levelForString(iLogLevel: iLogLevel)
                        lfPath = rootCachePath.appendingPathComponent("logs").appendingPathComponent("log").absoluteString
                    }
                } else {
                    // No! Then if ANY value is specified other than none, we'll use Meta instead.  Which is client safe.
                    if xcLogLevel != .none {
                        xcLogLevel = .meta
                    }
                }
            #endif
            babelfish.setup(level: xcLogLevel, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: lfPath as AnyObject?, fileLevel: lfLogLevel)
            self.displayLogLevel()
        }

        var partnerId: Int = -1
        var partnerSecret: String = ""
        var apiHost: String = ""
        
        guard let id: String = credentials["partnerID"] as? String else {
            return nil
        }
        
        guard let secret: String = credentials["partnerSecret"] as? String else {
            return nil
        }
        
        if let host: String = credentials["host"] as? String {
            apiHost = host
        }
        
        if let idNumber: Int = Int(id) {
            if idNumber > 0 {
                partnerId = idNumber
            } else {
                return nil
            }
        } else {
            return nil
        }
        
        if secret.characters.count > 0 {
            partnerSecret = secret
        } else {
            return nil
        }
        
        if apiHost.characters.count == 0 {
            apiHost = self.defaultAPIHost
        }
        
        self.partnerID = partnerId
        self.partnerSecret = partnerSecret
        self._apiHost = apiHost
        
        // set version or use default
        self.api = self.apiBase + "\(self.defaultAPIVersion)"
        
        // Init Map Cache Directory
        let finder = FileManager.default
        var rcp: URL
        do {
            let cacheDir: URL = try finder.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: false)
            rcp = cacheDir.appendingPathComponent("com.aisle411.venuekit")
            self.rootCachePath = rcp
            self.rootMapsCachePath = self.rootCachePath.appendingPathComponent("maps")
            self.searchSuggestionDBPath = self.rootCachePath.appendingPathComponent("suggestions")
            self.searchSuggestionPath = self.rootCachePath.appendingPathComponent("textSuggestions")
        } catch {
            rcp = URL(string: "/tmp/com.aisle411.venuekit/")!
            self.rootCachePath = rcp
            self.rootMapsCachePath = URL(string: "/tmp/com.aisle411.venuekit/maps/")!
            self.searchSuggestionDBPath = URL(string: "/tmp/com.aisle411.venuekit/suggestions/")!
            self.searchSuggestionPath = URL(string: "/tmp/com.aisle411.venuekit/textSuggestions/")!
        }
        
        self._credentials = credentials
        self.credentialed = true
        
        // Load dataCache
        self.dataCache = IVKDataCache(rootCacheURL: rcp)
        
        super.init()
        
        sharpenTheAxe(rootCachePath: self.rootCachePath)
        
        IVKObject.runOnMainThread {
            self.locationService = LocationService()
            self.locationService.start()
        }
    }
    
    // MARK:- public
    
    /**
     Method to get url to cached map bundle. If map hasn't cached it downloads map bundle first
     - parameter forVenueId: id of store
     - parameter withResponseBlock: completion callback
     */
    
    @objc
    public func requestCachedRasterMap(forVenueId id: Int, withResponseBlock block: @escaping ((_ path: URL?, _ errors: [IVKError]) -> ())) -> Bool {
        let methodName: String = "fetchVenueRasterMap(id: Int, block: ((path: URL?, errors: [IVKError]) -> ())) -> Bool"
        guard let location: CLLocation = self.IVKIsAvailable(methodName: methodName) else {
            return false
        }
        
        let entry: IVKDataCacheForVenue? = self.dataCache.dataCacheForVenue(id: id)
        let mapLMD: Date = entry!.imapEntry.lastModifiedDate
        
        let req: IVKRequestVenueRasterMap = IVKRequestVenueRasterMap(server: self, cacheRootPath: self.rootMapsCachePath)!
        req.useSSL = self.useSSL
        let params: IVKRequestVenueRasterMapParameters = IVKRequestVenueRasterMapParameters(deviceLocation: location, venueId: id)
        req.executeRequest(lastModifiedDate: mapLMD, params: params, reply: { (result) -> () in
            if let results: IVKResultVenueRasterMap = result as? IVKResultVenueRasterMap {
                var errors: [IVKError] = []
                if results.hasErrors {
                    errors = results.errors
                }
                
                let path: URL = results.cachedMapPath
                
                block(path, errors)
                
                if let lMD: Date = result.lastModifiedDate {
                    var entry: IVKDataCacheForVenue? = self.dataCache.dataCacheForVenue(id: id)
                    babelfish.info("Last Modified Date of new IMAP Map: \(lMD)")
                    entry!.imapEntry.lastModifiedDate = lMD
                    self.dataCache.updateDataCacheForVenue(dCache: entry!)
                    entry = nil
                }
            } else {
                // For completeness
                block(nil, [IVKError(errorType: IVKErrorType.generalError)])
                self.ohNo(methodName: methodName, params: ["venueId":"\(id)" as AnyObject, "deviceLocation": location as AnyObject])
            }
        })
        return true
    }
    
    //MARK: Search for items in venues using search terms, UPCs, or dictionaries.

    
    /**
     Method to search product by a search term
     - parameter venueWithId: id of store
     - parameter searchTerm: search term to find product
     - parameter withStartingIndex: first product index in a total search result. Can be used to implement pagination
     - parameter andEndingIndex: last product index in a total search result. Can be used to implement pagination
     - parameter withMaxCount: max sections count per product
     - parameter withResponseBlock: result callback
     */
    @objc
    public func search(venueWithId venueId: Int, forTerm searchTerm: String, withStartingIndex start: Int, andEndingIndex end: Int, withMaxCount maxCount: Int, withResponseBlock block: @escaping ((_ venueItems: [IVKVenueItem], _ errors: [IVKError]) -> ())) -> Bool {
        return self.search(venueId: venueId, searchTerm: searchTerm, isUPC: false, start: start, end: end, maxCount: maxCount, block: block)
    }
    
    @objc
    public func search(venueWithId venueId: Int, forTerm searchTerm: String, withStartingIndex start: Int, andEndingIndex end: Int, withMaxCount maxCount: Int, customHeaders: [String: String], withResponseBlock block: @escaping ((_ venueItems: [IVKVenueItem], _ errors: [IVKError]) -> ())) -> Bool {
        return self.search(venueId: venueId, searchTerm: searchTerm, isUPC: false, start: start, end: end, maxCount: maxCount, customHeaders: customHeaders, block: block)
    }
    
    /**
     Method to search product by UPC
     - parameter venueWithId: id of store
     - parameter forUPC: product UPC
     - parameter withStartingIndex: first product index in a total search result. Can be used to implement pagination
     - parameter andEndingIndex: last product index in a total search result. Can be used to implement pagination
     - parameter withMaxCount: max sections count per product
     - parameter withResponseBlock: result callback
     */
    @objc
    public func search(venueWithId venueId: Int, forUPC upc: String, withStartingIndex start: Int, andEndingIndex end: Int, withMaxCount maxCount: Int, withResponseBlock block: @escaping ((_ venueItems: [IVKVenueItem], _ errors: [IVKError]) -> ())) -> Bool {
        return self.search(venueId: venueId, searchTerm: upc, isUPC: true, start: start, end: end, maxCount: maxCount, block: block)
    }
    
    /**
     Method to search product by UPC
     - parameter venueWithId: id of store
     - parameter forUPC: product UPC
     - parameter withStartingIndex: first product index in a total search result. Can be used to implement pagination
     - parameter andEndingIndex: last product index in a total search result. Can be used to implement pagination
     - parameter withMaxCount: max sections count per product
     - parameter customHeaders: headers to pass with request
     - parameter withResponseBlock: result callback
     */
    internal func search(venueId: Int, searchTerm: String, isUPC: Bool = false, start: Int, end: Int, maxCount: Int, customHeaders: [String: String]? = nil, block: @escaping ((_ venueItems: [IVKVenueItem], _ errors: [IVKError]) -> ())) -> Bool {
        let methodName: String = "fetchVenueItems(venueId: Int, searchTerm: String, isUPC: Bool = false, start: Int, end: Int, maxCount: Int, block: (venueItems: [IVKVenueItem], errors: [IVKError]) -> ())) -> Bool"
        guard let location: CLLocation = self.IVKIsAvailable(methodName: methodName) else {
            return false
        }
        
        let req: IVKRequestSearchVenueItem = IVKRequestSearchVenueItem(server: self)!
        req.useSSL = self.useSSL
        var params: IVKRequestSearchVenueItemParameters
        if isUPC {
            params = IVKRequestSearchVenueItemParameters(deviceLocation: location, venueId: venueId, searchUPC: searchTerm, start: start, end: end)
        } else {
            params = IVKRequestSearchVenueItemParameters(deviceLocation: location, venueId: venueId, searchTerm: searchTerm, start: start, end: end)
        }
        params.maxLocations = maxCount
        params.customHeaders = customHeaders
        req.executeRequest(params: params, reply: { (result) -> () in
            if let results: IVKResultVenueItem = result as? IVKResultVenueItem {
                var errors: [IVKError] = []
                if results.hasErrors {
                    errors = results.errors
                }
                
                var venueItems: [IVKVenueItem] = []
                if results.venueItems.count > 0 {
                    for item in results.venueItems {
                        if item.normalizedName.length > 0 {
                            venueItems.append(item)
                        }
                    }
                }
                
                block(venueItems, errors)
            } else {
                // For completeness
                block([], [IVKError(errorType: IVKErrorType.generalError)])
                self.ohNo(methodName: methodName, params: ["venueId": venueId as AnyObject, "searchTerm": searchTerm as AnyObject, "isUPC": isUPC as AnyObject, "start": start as AnyObject, "end": end as AnyObject, "maxCount": maxCount as AnyObject, "deviceLocation": location as AnyObject])
            }
        })
        return true
    }
    
    /**
     Method to search shopping list content
     - parameter venueWithId: id of store
     - parameter forItemsFromDictionary: dictionary with shopping list items attributes
     - parameter withResponseBlock: result callback
     */
    @objc
    public func search(venueWithId venueId: Int, forItemsFromDictionary searchDictionary: [String: AnyObject], withResponseBlock block: @escaping ((_ venueItems: [IVKVenueItem], _ errors: [IVKError]) -> ())) -> Bool {
        let methodName: String = "fetchVenueItems(venueId: Int, list: IVKSearchList, block: ((venueItems: [IVKVenueItem]?, errors: [IVKError]) -> ())) -> Bool"
        guard let location: CLLocation = self.IVKIsAvailable(methodName: methodName) else {
            return false
        }
        
        let req: IVKRequestLocateItems = IVKRequestLocateItems(server: self)!
        req.useSSL = self.useSSL
        let params: IVKRequestLocateItemsParameters = IVKRequestLocateItemsParameters(deviceLocation: location, venueId: venueId, list: searchDictionary)
        
        req.executeRequest(params: params, reply: { (result) -> () in
            if let results: IVKResultVenueItem = result as? IVKResultVenueItem {
                var errors: [IVKError] = []
                if results.hasErrors {
                    errors = results.errors
                }
                
                var venueItems: [IVKVenueItem] = []
                if results.venueItems.count > 0 {
                    for item in results.venueItems {
                        if item.normalizedName.length > 0 {
                            venueItems.append(item)
                        }
                    }
                }
                
                block(venueItems, errors)
            } else {
                // For completeness
                block([], [IVKError(errorType: IVKErrorType.generalError)])
                self.ohNo(methodName: methodName, params: ["venueId": venueId as AnyObject, "searchDictionary": searchDictionary as AnyObject, "deviceLocation": location as AnyObject])
            }
        })
        return true
        
    }
    
    /**
     Method to check for updates to the search suggestions. If an update is available, it is downloaded and cached. MUST be called prior to calling suggestionsFor(text:, includeContained:)
     - parameter forVenueWithId: id of store
     - parameter withResponseBlock: result callback
     */
    @objc
    public func refreshSearchSuggestions(forVenueWithId venueId: Int, withResponseBlock block: @escaping ((_ suggestionsAvailable: Bool, _ errors: [IVKError]) -> ())) -> Bool {
        return self.fetchSearchSuggestions(venueId: venueId, block: {(suggestions: IVKSuggestions?, errors: [IVKError]) -> () in
            if let suggs = suggestions {
                self.suggestions = suggs
                block(true, errors)
            } else {
                block(false, errors)
            }
        })
    }
    
    /**
     Method to return a list of suggested terms beginning with the passed trimmed text.  If search suggestions are not available, an empty array will be returned.
     - parameter text: text to search suggestions
     - parameter includeContained: if true, then result will be cobntain terms that contain text overvise terms that starts with provided text
     */
    @objc
    public func suggestions(for text: String, includeContained: Bool = false) -> [String] {
        guard let suggs: IVKSuggestions = self.suggestions else {
            babelfish.meta("Suggestions could not be found.  This can be caused by not having not having called refreshSearchSuggestions or there are no suggestions available.")
            return []
        }
        return suggs.suggestionsFor(text: text, includeContained: includeContained)
    }
    
    internal func fetchSearchSuggestions(venueId: Int, block: @escaping ((_ suggestions: IVKSuggestions?, _ errors: [IVKError]) -> ())) -> Bool {
        // make request, cached in result, IVKApp and IVMKMapView have simple interface
        let methodName: String = "fetchSearchSuggestions(venueId: Int, block: ((errors: [IVKError]) -> ()))"
        guard let location: CLLocation = self.IVKIsAvailable(methodName: methodName) else {
            return false
        }
        
        // 0 will be used for search suggestions for now since they are done by partner.
        let entry: IVKDataCacheForVenue? = self.dataCache.dataCacheForVenue(id: 0)
        let sLMD: Date = entry!.searchSuggestionTextEntry.lastModifiedDate
        babelfish.info("Last Modified Date of current Suggestion DB: \(sLMD)")
        
        let req: IVKRequestSuggestionText = IVKRequestSuggestionText(server: self, cacheRootPath: self.searchSuggestionPath)!
        req.useSSL = self.useSSL
        let params: IVKRequestVenueInfoParameters = IVKRequestVenueInfoParameters(deviceLocation: location, venueId: venueId)
        req.executeRequest(lastModifiedDate: sLMD, params: params, reply: { (result) -> () in
            if let results: IVKResultSuggestionText = result as? IVKResultSuggestionText {
                var errors: [IVKError] = []
                if results.hasErrors {
                    errors = results.errors
                }
                
                if let suggs: IVKSuggestions = results.suggestions {
                    block(suggs, errors)
                } else {
                    block(nil, errors)
                }
                
                if let lMD: Date = result.lastModifiedDate {
                    var entry: IVKDataCacheForVenue? = self.dataCache.dataCacheForVenue(id: 0)
                    babelfish.info("Last Modified Date of new Suggestion Text: \(lMD)")
                    entry!.searchSuggestionTextEntry.lastModifiedDate = lMD
                    self.dataCache.updateDataCacheForVenue(dCache: entry!)
                    entry = nil
                }
            } else {
                // For completeness
                block(nil, [IVKError(errorType: IVKErrorType.generalError)])
                self.ohNo(methodName: methodName, params: ["deviceLocation": location as AnyObject, "venueId": venueId as AnyObject])
            }
        })
        return true
    }
    
    //MARK: Request venues based on location and optionally a search term or UPC.
    
    /**
     Method to return a shop list based on provided location
     - parameter providedLocation: location to search
     - parameter startingWithIndex: first store index in a total result list. Can be used to implement pagination
     - parameter endingWithIndex: last store index in a total result list. Can be used to implement pagination
     - parameter withResponseBlock: result callback
     */
    @objc
    public func requestVenues(near providedLocation: CLLocation? = nil, startingWithIndex start: Int, endingWithIndex end: Int, withResponseBlock block: @escaping ((_ venues: [IVKVenue]?, _ errors: [IVKError]) -> ())) -> Bool {
        let methodName: String = "fetchVenues(start: Int, end: Int, location: CLLocation? = nil, block: ((venues: [IVKVenue]?, errors: [IVKError]) -> ())) -> Bool"
        guard let location: CLLocation = self.IVKIsAvailable(methodName: methodName) else {
            return false
        }
        
        var searchLocation: CLLocation = location
        if let _ = providedLocation {
            searchLocation = providedLocation!
        }
        
        let req: IVKRequestVenues = IVKRequestVenues(server: self)!
        req.useSSL = self.useSSL
        let params: IVKRequestVenuesParameters = IVKRequestVenuesParameters(deviceLocation: location, location: searchLocation, start: start, end: end)
        req.executeRequest(params: params, reply: { (result) -> () in
            if let results: IVKResultVenue = result as? IVKResultVenue {
                var errors: [IVKError] = []
                if results.hasErrors {
                    errors = results.errors
                }
                
                var venues: [IVKVenue]?
                if results.venues.count > 0 {
                    venues = results.venues
                }
                
                block(venues, errors)
            } else {
                // For completeness
                block(nil, [IVKError(errorType: IVKErrorType.generalError)])
                self.ohNo(methodName: methodName, params: ["start": start as AnyObject, "end": end as AnyObject, "searchLocation": searchLocation as AnyObject, "deviceLocation": location as AnyObject])
            }
        })
        return true
    }
    
    // MARK:- Internal
    

    
    //MARK: - Private Methods
    
    private func ohNo(methodName: String, params: [String: AnyObject]) {
        babelfish.severe("ATTENTION: An error occured that requires the attention of Aisle411.  Please note the credentials used to execute this request and this message and share with Aisle411.  \n - IVKRequest.executeRequest (with block) failed to return the proper sub class of IVKResult. \n - AisleServer.\(methodName) \n - params: \(params)")
    }
    
    private func IVKIsAvailable(methodName: String) -> CLLocation? {
        if !self.credentialed {
            babelfish.info("AisleServer.\(methodName) attempted to access Aisle411 APIs with uncredentialed AisleServer.  Valid Aisle411 IVK Credentials are required to access Aisle411 APIs.")
            return nil
        }
        
        return self.locationService.deviceLocation
    }
    
    internal func displayLogLevel() {
        babelfish.meta("Meta logging enabled.")
        babelfish.severe("Severe logging enabled.")
        babelfish.error("Error logging enabled.")
        babelfish.warning("Warning logging enabled.")
        babelfish.info("Info logging enabled.")
        babelfish.debug("Debug logging enabled.")
        babelfish.verbose("Verbose logging enabled.")
    }
}
