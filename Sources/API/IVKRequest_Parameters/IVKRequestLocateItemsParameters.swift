//
//  IVKRequestLocateItemsParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestLocateItems to fetch venue item search results in an IVKResultVenueItem object.
*/
internal class IVKRequestLocateItemsParameters: IVKRequestVenueInfoParameters {
    internal let kList: String = "shopping_list_data"
    /// Dictionary of shopping list items.
    internal var list: [String: AnyObject]!
    
    /**
    - parameter deviceLocation: (CLLocation) location of device at time of request.
    - parameter storeId: (Int) Aisle411 assigned unique id for store (ISKStore.id)
    - parameter list: ([String: AnyObject]) Dictionary of search items.
    */
    internal init(deviceLocation: CLLocation, venueId: Int, list: [String: AnyObject]) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
        self.list = list
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestLocateItems.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kList] = self.list as AnyObject?
        return params
    }
}

