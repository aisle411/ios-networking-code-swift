//
//  IVKRequestPartnerVenueParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestPartnerVenue to fetch an array of one store in an IVKResultVenue object.
*/
internal class IVKRequestPartnerVenueParameters: IVKRequestParameters {
    internal let kVendorVenueId: String = "vendor_store_nbr"
    
    /// Store number provided by vendor.
    internal var vendorVenueId: Int
    
    /**
    - parameter deviceLocation: (CLLocation) location of device at time of request.
    - parameter vendorStoreNumber: (String) internal number of store from vendor.
    */
    internal init(deviceLocation: CLLocation, vendorVenueId: Int) {
        self.vendorVenueId = vendorVenueId
        super.init(deviceLocation: deviceLocation)
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestPartnerVenue.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kVendorVenueId] = "\(self.vendorVenueId)" as AnyObject?
        return params
    }
}
