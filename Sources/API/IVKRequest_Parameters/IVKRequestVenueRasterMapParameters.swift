//
//  IVKRequestVenueRasterMapParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestVenueRasterMap to fetch an a store map in an IVKResultVenueRasterMap object.
*/
internal class IVKRequestVenueRasterMapParameters: IVKRequestVenueInfoParameters {
    internal let kLatitude: String = "latitude"
    internal let kLongitude: String = "longitude"
    
    /**
    - parameter deviceLocation: (CLLocation) location of device at time of request.
    - parameter storeId: (Int) Aisle411 assigned unique id for store (ISKStore.id)
    */
    override internal init(deviceLocation: CLLocation, venueId: Int) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestVenueRasterMap.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kLatitude] = "\(self.deviceLatitude)" as AnyObject?
        params[self.kLongitude] = "\(self.deviceLongitude)" as AnyObject?
        return params
    }
}
