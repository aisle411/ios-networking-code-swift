//
//  IVKRequestMetricLocationParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/22/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestMetricLocation.
*/
internal class IVKRequestMetricLocationParameters: IVKRequestParameters {
    internal let kX: String = "x"
    internal let kY: String = "y"
    internal let kStoreId: String = "retailer_store_id"
    internal let kFloor: String = "floor_level"
    internal let kAccuracy: String = "accuracy"
    internal let kHeading: String = "heading"
    private let kTimeStamp: String = "timestamp"
    private let kMetricType: String = "imetric_type_cd"
    
    internal let kNodeX: String = "node_x"
    internal let kNodeY: String = "node_y"
    internal let kNodeName: String = "node_name"
    
    internal let kZoneName: String = "zone_name"
    
    private let metricType: String = "USERLOC"
    
    internal var x: Double = 0.0
    internal var y: Double = 0.0
    internal var accuracy: Double = 1000.0
    internal var heading: Double = 360.0
    internal var floor: Int = 0
    internal var storeId: Int = -1
    internal var nodeX: Float = 0
    internal var nodeY: Float = 0
    internal var nodeName: String = ""
    internal var zoneName: String = ""
    
    private let now: Date = Date()
    internal var timestamp: String {
        get {
            let f: DateFormatter = DateFormatter()
            f.dateFormat = "YYYY-MM-dd HH:mm:ss"
            return f.string(from: self.now)
        }
    }
    internal init(deviceLocation: CLLocation, storeId: Int) {
        super.init(deviceLocation: deviceLocation)
        self.storeId = storeId
    }
    
    internal init(deviceLocation: CLLocation, storeId: Int, x: Double, y: Double, heading: Double, floor: Int, accuracy: Double) {
        super.init(deviceLocation: deviceLocation)
        self.storeId = storeId
        self.x = x
        self.y = y
        self.heading = heading
        self.floor = floor
        self.accuracy = accuracy
        
    }
    
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kMetricType] = self.metricType as AnyObject?
        params[self.kStoreId] = "\(self.storeId)" as AnyObject?
        params[self.kX] = "\(self.x)" as AnyObject?
        params[self.kY] = "\(self.y)" as AnyObject?
        params[self.kHeading] = "\(self.heading)" as AnyObject?
        params[self.kFloor] = "\(self.floor)" as AnyObject?
        params[self.kAccuracy] = "\(self.accuracy)" as AnyObject?
        params[self.kTimeStamp] = "\(self.timestamp)" as AnyObject?
        params[self.kNodeName] = "\(self.nodeName)" as AnyObject?
        params[self.kNodeX] = "\(self.nodeX)" as AnyObject?
        params[self.kNodeY] = "\(self.nodeY)" as AnyObject?
        params[self.kZoneName] = "\(self.zoneName)" as AnyObject?
        
        return params
    }
}
