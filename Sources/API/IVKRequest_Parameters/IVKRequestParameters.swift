//
//  IVKRequestParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 8/14/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
    Protocol for IVKRequestParameters.
*/
internal protocol IVKRequestParametersProtocol: NSObjectProtocol {
    func paramArray() -> [String: AnyObject]
}

/**
    Base class for IVKRequestParameters subclasses.
*/
internal class IVKRequestParameters: IVKObject, IVKRequestParametersProtocol {
    internal let kDeviceLatitude: String = "device_latitude"
    internal let kDeviceLongitude: String = "device_longitude"
    internal var deviceLocation: CLLocation!
    
    /// Latitude of device at time of request.
    internal var deviceLatitude: Double {
        get {
            return self.deviceLocation.coordinate.latitude
        }
    }
    
    /// Longitude of device at time of request.
    internal var deviceLongitude: Double {
        get {
            return self.deviceLocation.coordinate.longitude
        }
    }
    
    internal init(deviceLocation: CLLocation) {
        self.deviceLocation = deviceLocation
    }
    
    /**
    Used by the IVKRequest object to assemble the API request.
    - returns: array output of parameters for the request.
    */
    internal func paramArray() -> [String: AnyObject] {
        var params:[String: AnyObject] = [String: AnyObject]()
        params[self.kDeviceLatitude] = "\(self.deviceLatitude)" as AnyObject?
        params[self.kDeviceLongitude] = "\(self.deviceLongitude)" as AnyObject?
        return params
    }
}
