import Foundation

internal protocol IVKBindableProtocol {
    func jsonDict() -> [NSString: Any]
}

class IVKRequestBindParameters: IVKObject, IVKRequestParametersProtocol {
    
    private var bindings: [IVKBindableProtocol]
    private var accountId: Int
    
    internal init(accountId: Int, bindings: [IVKBindableProtocol]) {
        self.bindings = bindings
        self.accountId = accountId
    }
    
    internal func paramArray() -> [String : AnyObject] {
        let bindingsJson = self.bindings.map { $0.jsonDict() }
        
        var params:[String: AnyObject] = [String: AnyObject]()
        params[IVKRequestBindBase.Constants.bindingsKey] = bindingsJson as AnyObject?
        params[IVKRequestBindBase.Constants.accountIdKey] = "\(accountId)" as AnyObject?
        return params
    }
}
