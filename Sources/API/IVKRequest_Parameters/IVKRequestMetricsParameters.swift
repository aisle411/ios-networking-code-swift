//
//  IVKRequestMetricsParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 3/9/16.
//  Copyright © 2016 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

internal class IVKRequestMetricsParameters: IVKRequestParameters {
    internal let kStoreId: String = "retailer_store_id"
    internal let kMetrics: String = "metrics"
    
    internal var storeId: Int = -1
    internal var metrics: [[String: AnyObject]] = []

    internal init(deviceLocation: CLLocation, storeId: Int) {
        super.init(deviceLocation: deviceLocation)
        self.storeId = storeId
    }
    
    internal init(deviceLocation: CLLocation, storeId: Int, metrics: [[String: AnyObject]]) {
        super.init(deviceLocation: deviceLocation)
        self.storeId = storeId
        self.metrics = metrics
    }
    
    override internal func paramArray() -> [String : AnyObject] {
        
        let opt: JSONSerialization.WritingOptions = .prettyPrinted
        var params:[String : AnyObject] = super.paramArray()
        params[self.kStoreId] = "\(self.storeId)" as AnyObject?
        params[self.kMetrics] = metrics as AnyObject?
//        do {
//            var tmp = try JSONSerialization.data(withJSONObject: metrics, options: opt) as AnyObject?
//            if let JSONString = String(data: tmp as! Data, encoding: String.Encoding.ascii) {
//                params[self.kMetrics] = tmp
//            }
//        } catch {
//           // ?
//        }

        
        return params
    }

}
