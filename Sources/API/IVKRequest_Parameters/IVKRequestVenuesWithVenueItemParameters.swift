//
//  IVKRequestVenuesWithVenueItemParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestVenuesWithVenueItem to fetch an array of stores nearby in an IVKResultVenueWithVenueItem object.
*/
internal class IVKRequestVenuesWithVenueItemParameters: IVKRequestParameters {
    internal let kLatitude: String = "latitude"
    internal let kLongitude: String = "longitude"
    internal let kTerm: String = "term"
    internal let kUPC: String = "upc"
    internal let kDistance: String = "distance"
    internal let kMaxVenues: String = "max_stores"
    
    /// Location for search.  Note, this is the source value for the latitude and longitude properties.
    internal var location: CLLocation!
    
    /// Latitude for the search.
    internal var latitude: Double {
        get {
            return self.location.coordinate.latitude
        }
    }
    
    /// Longitude for the search.
    internal var longitude: Double {
        get {
            return self.location.coordinate.longitude
        }
    }
    
    /// Whether to use keyword search (false indicates usage of UPC search)
    internal var useSearchTerm: Bool = true
    /// Whether to use UPC search (false indicates usage of keyword search)
    internal var useUPC: Bool {
        get {
            return !self.useSearchTerm
        }
        set {
            self.useSearchTerm = !newValue
        }
    }
    private var term: String = ""
    /// Keyword for usage in keyword search.
    internal var searchTerm: String? {
        get {
            if self.useSearchTerm {
                return self.term
            }
            return nil
        }
        set {
            if let newTerm: String = newValue {
                self.useSearchTerm = true
                self.term = newTerm
            }
        }
    }
    /// UPC for usage in UPC search
    internal var upc: String? {
        get {
            if self.useUPC {
                return self.term
            }
            return nil
        }
        set {
            if let newTerm: String = newValue {
                self.useSearchTerm = false
                self.term = newTerm
            }
        }
    }
    
    /// Radius of search in Miles
    internal var distance: Float = 15.0
    /// Maximum number of stroes to be returned
    internal var maxVenues: Int = 25
    
    /**
    Short init for keyword use with default properties or setting of other properties after init.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter location: (CLLocation) location to base search for nearby stores.
    - parameter searchTerm: (String) keyword for search
    */
    internal init(deviceLocation: CLLocation, location: CLLocation, searchTerm: String) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
        self.useSearchTerm = true
        self.term = searchTerm
    }

    /**
    Short init for UPC use with default properties or setting of other properties after init.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter location: (CLLocation) location to base search for nearby stores.
    - parameter searchUPC: (String) UPC for search.
    */
    internal init(deviceLocation: CLLocation, location: CLLocation, searchUPC: String) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
        self.useUPC = true
        self.term = searchUPC
    }
    
    /**
    Long init for setting up the params for a keyword search with a single line of code.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter location: (CLLocation) location to base search for nearby stores.
    - parameter searchTerm: (String) keyword for search.
    - parameter distance: (Float) radius of search area.
    - parameter maxStores: (Int) maximum number of stores to return.
    */
    internal init(deviceLocation: CLLocation, location: CLLocation, searchTerm: String, distance: Float, maxVenues: Int) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
        self.useSearchTerm = true
        self.term = searchTerm
        self.distance = distance
        self.maxVenues = maxVenues
    }
    
    /**
    Long init for setting up the params for a UPC search with a single line of code.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter location: (CLLocation) location to base search for nearby stores.
    - parameter searchUPC: (String) UPC for search.
    - parameter distance: (Float) radius of search area.
    - parameter maxStores: (Int) maximum number of stores to return.
    */
    internal init(deviceLocation: CLLocation, location: CLLocation, searchUPC: String, distance: Float, maxVenues: Int) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
        self.useUPC = true
        self.term = searchUPC
        self.distance = distance
        self.maxVenues = maxVenues
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestVenuesWithVenueItem.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kLatitude] = "\(self.latitude)" as AnyObject?
        params[self.kLongitude] = "\(self.longitude)" as AnyObject?
        params[self.kDistance] = "\(self.distance)" as AnyObject?
        params[self.kMaxVenues] = "\(self.maxVenues)" as AnyObject?
        if self.useSearchTerm {
            params[self.kTerm] = self.searchTerm as AnyObject?
        } else if self.useUPC {
            params[self.kUPC] = self.upc as AnyObject?
        } else {
            // This shall never execute, but is provided simply to satisfy completeness in the event of a bug in the term/upc logic.
            params[self.kTerm] = self.searchTerm as AnyObject?
        }
        return params
    }
}
