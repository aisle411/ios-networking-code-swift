import Foundation
import CoreLocation

internal class IVKVenuesForAccountRequestParameters: IVKObject, IVKRequestParametersProtocol {
    
    internal struct Constants {
        static let accountId = "account_id"
        static let latitude  = "latitude"
        static let longitude = "longitude"
        static let start     = "start"
        static let end       = "end"
    }
    
    internal var accountId: Int
    internal var location: CLLocation?
    internal var start: Int
    internal var end: Int
    
    internal init(accountId: Int, location: CLLocation?, start: Int, end: Int) {
        self.accountId = accountId
        self.location = location
        self.start = start
        self.end = end
    }
    
    internal func paramArray() -> [String : AnyObject] {
        var params:[String: AnyObject] = [String: AnyObject]()
        params[Constants.accountId] = "\(self.accountId)" as AnyObject
        if let location = self.location {
            params[Constants.latitude] = "\(location.coordinate.latitude)" as AnyObject
            params[Constants.longitude] = "\(location.coordinate.longitude)" as AnyObject
        }
        params[Constants.start] = "\(self.start)" as AnyObject
        params[Constants.end] = "\(self.end)" as AnyObject
        return params
    }
}
