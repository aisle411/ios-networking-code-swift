//
//  IVKRequestSigninParameters.swift
//  IVKLive
//
//  Created by Dante Cannarozzi on 8/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Parameters class for use with IVKRequestSigninParameters to fetch account information and a list of venues in an IVKResultAccount object.
 */
internal class IVKRequestSigninParameters: IVKRequestParameters {
    internal let kVendorEmail: String = "e-mail"
    internal let kVendorPassword: String = "password"
    
    /// email provided by vendor.
    internal var vendorEmail: String
    
    /// password provided by vendor.
    internal var vendorPassword: String
    
    /**
     - parameter deviceLocation: (CLLocation) location of device at time of request.
     - parameter vendorStoreNumber: (String) internal number of store from vendor.
     */
    internal init(deviceLocation: CLLocation, email: String, password: String) {
        self.vendorEmail = email
        self.vendorPassword = password
        super.init(deviceLocation: deviceLocation)
    }
    
    /**
     Override of base paramArray() method.
     Used by IVKRequestSignin.
     */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kVendorEmail] = self.vendorEmail as AnyObject?
        params[self.kVendorPassword] = self.vendorPassword as AnyObject?
        return params
    }
}
