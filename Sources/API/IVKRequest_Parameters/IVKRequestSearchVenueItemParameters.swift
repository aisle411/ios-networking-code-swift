//
//  IVKRequestSearchVenueItemParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestSearchVenueItem to fetch venue item search results in an IVKResultVenueItem object.
*/
internal class IVKRequestSearchVenueItemParameters: IVKRequestVenueInfoParameters {
    internal let kTerm: String = "term"
    internal let kUPC: String = "upc"
    internal let kStart: String = "start"
    internal let kEnd: String = "end"
    internal let kMaxLocations: String = "max_locations"
    internal var start: Int = 0
    internal var end: Int = 24
    internal var maxLocations: Int = 3
    internal var customHeaders: [String: String]?
    
    /// Whether to use keyword search (false indicates usage of UPC search)
    internal var useSearchTerm: Bool = true
    /// Whether to use UPC search (false indicates usage of keyword search)
    internal var useUPC: Bool {
        get {
            return !self.useSearchTerm
        }
        set {
            self.useSearchTerm = !newValue
        }
    }
    private var term: String = ""
    /// Keyword for usage in keyword search.
    internal var searchTerm: String? {
        get {
            if self.useSearchTerm {
                return self.term
            }
            return nil
        }
        set {
            if let newTerm: String = newValue {
                self.useSearchTerm = true
                self.term = newTerm
            }
        }
    }
    /// UPC for usage in UPC search
    internal var upc: String? {
        get {
            if self.useUPC {
                return self.term
            }
            return nil
        }
        set {
            if let newTerm: String = newValue {
                self.useSearchTerm = false
                self.term = newTerm
            }
        }
    }
    
    /**
    Short init for keyword use with default properties or setting of other properties after init.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter storeId: (Int) Aisle411 assigned unique id for store (ISKStore.id)
    - parameter searchTerm: (String) keyword for search
    */
    internal init(deviceLocation: CLLocation, venueId: Int, searchTerm: String) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
        self.venueId = venueId
        self.searchTerm = searchTerm
    }
    
    /**
    Short init for UPC use with default properties or setting of other properties after init.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter storeId: (Int) Aisle411 assigned unique id for store (ISKStore.id)
    - parameter searchUPC: (String) UPC for search.
    */
    internal init(deviceLocation: CLLocation, venueId: Int, searchUPC: String) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
        self.venueId = venueId
        self.upc = searchUPC
    }
    
    /**
    Long init for setting up the params for a keyword search with a single line of code.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter searchTerm: (String) keyword for search.
    - parameter distance: (Float) radius of search area.
    - parameter start: (Int) record to start results of search. (1 based)
    - parameter end: (Int) record to end results of search. (1 based)
    */
    internal init(deviceLocation: CLLocation, venueId: Int, searchTerm: String, start: Int, end: Int) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
        self.venueId = venueId
        self.searchTerm = searchTerm
        self.start = start
        self.end = end
    }
    
    /**
    Long init for setting up the params for a UPC search with a single line of code.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter searchUPC: (String) UPC for search.
    - parameter distance: (Float) radius of search area.
    - parameter start: (Int) record to start results of search. (1 based)
    - parameter end: (Int) record to end results of search. (1 based)
    */
    internal init(deviceLocation: CLLocation, venueId: Int, searchUPC: String, start: Int, end: Int) {
        super.init(deviceLocation: deviceLocation, venueId: venueId)
        self.venueId = venueId
        self.upc = searchUPC
        self.start = start
        self.end = end
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestSearchVenueItem.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kMaxLocations] = "\(self.maxLocations)" as AnyObject?
        params[self.kStart] = "\(self.start)" as AnyObject?
        params[self.kEnd] = "\(self.end)" as AnyObject?
        if self.useSearchTerm {
            params[self.kTerm] = self.searchTerm as AnyObject?
        } else if self.useUPC {
            params[self.kUPC] = self.upc as AnyObject?
        } else {
            // This shall never execute, but is provided simply to satisfy completeness in the event of a bug in the term/upc logic.
            params[self.kTerm] = self.searchTerm as AnyObject?
        }
        
        self.customHeaders?.forEach({ (key, value) in
            params[key] = value as AnyObject?
        })
        
        return params
    }
}
