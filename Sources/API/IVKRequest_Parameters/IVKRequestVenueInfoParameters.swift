//
//  IVKRequestVenueInfoParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestVenueInfo to fetch an array of one store in an IVKResultVenue object.
*/
internal class IVKRequestVenueInfoParameters: IVKRequestParameters {
    internal let kVenueId: String = "retailer_store_id"
    
    /// Aisle411 assigned unique id for store (ISKStore.id)
    internal var venueId: Int
    
    /**
    - parameter deviceLocation: (CLLocation) location of device at time of request.
    - parameter storeId: (Int) Aisle411 assigned unique id for store (ISKStore.id)
    */
    internal init(deviceLocation: CLLocation, venueId: Int) {
        self.venueId = venueId
        super.init(deviceLocation: deviceLocation)
    }
    
    internal init(deviceLocation: CLLocation, venueId: Int, className: String) {
        self.venueId = venueId
        super.init(deviceLocation: deviceLocation)
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestVenueInfo.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kVenueId] = "\(self.venueId)" as AnyObject?
        return params
    }
}
