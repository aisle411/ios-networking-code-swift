//
//  IVKRequestConvertCoordinateParameters.swift
//  VenueKit
//
//  Created by Minxin Guo on 11/3/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation
import CoreLocation

class IVKRequestConvertCoordinateParameters: IVKRequestParameters {
    
    var x: Float
    var y: Float
    var venueId: Int
    
    /**
     - parameter deviceLocation: (CLLocation) location of device at time of request.
     - parameter x: coordiante of x that needs to be converted
     - parameter y: coordiante of y that needs to be converted
     - parameter venueId: venueId of the store to use
     */
    init(deviceLocation: CLLocation, x: Float, y: Float, venueId: Int) {
        self.x = x
        self.y = y
        self.venueId = venueId
        super.init(deviceLocation: deviceLocation)
    }
    
    /**
     Override of base paramArray() method.
     Used by IVKRequestCoordianteConversion.
     */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params["x"] = "\(x)" as AnyObject?
        params["y"] = "\(y)" as AnyObject?
        params["retailer_store_id"] = "\(venueId)" as AnyObject?
        return params
    }
}
