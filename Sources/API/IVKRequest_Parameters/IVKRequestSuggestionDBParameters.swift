//
//  IVKRequestSuggestionDBParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 12/9/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Parameters class for use with IVKRequestPartnerVenue to fetch an array of one store in an IVKResultVenue object.
 */
internal class IVKRequestSuggestionDBParameters: IVKRequestParameters {
    /**
     - parameter deviceLocation: (CLLocation) location of device at time of request.
     - parameter vendorStoreNumber: (String) internal number of store from vendor.
     */
    internal override init(deviceLocation: CLLocation) {
        super.init(deviceLocation: deviceLocation)
    }
    
    /**
     Override of base paramArray() method.
     Used by IVKRequestPartnerVenue.
     */
     override internal func paramArray() -> [String : AnyObject] {
        let params:[String : AnyObject] = super.paramArray()
        return params
    }
}