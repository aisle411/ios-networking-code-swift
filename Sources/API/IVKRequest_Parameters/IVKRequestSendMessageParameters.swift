//
//  IVKRequestSendMessageParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestSendMessage.
*/
internal class IVKRequestSendMessageParameters: IVKRequestParameters {
    internal let kMessage: String = "message"
    /// Dictionary of search items.
    internal var message: [String: AnyObject]!
    
    /**
    - parameter deviceLocation: (CLLocation) location of device at time of request.
    - parameter message: ([String: AnyObject]) Dictionary of search items.
    */
    internal init(deviceLocation: CLLocation, message: [String: AnyObject]) {
        super.init(deviceLocation: deviceLocation)
        self.message = message
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestSendMessage.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kMessage] = self.message as AnyObject?
        return params
    }
}

