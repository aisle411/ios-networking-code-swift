//
//  IVKRequestVenuesParameters.swift
//  VenueKit
//
//  Created by Fred Priese on 9/8/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation
import CoreLocation

/**
Parameters class for use with IVKRequestVenues to fetch an array of stores nearby in an IVKResultVenue object.
*/
internal class IVKRequestVenuesParameters: IVKRequestParameters {
    internal let kLatitude: String = "latitude"
    internal let kLongitude: String = "longitude"
    internal let kStart: String = "start"
    internal let kEnd: String = "end"
    
    /// Location for search.  Note, this is the source value for the latitude and longitude properties.
    internal var location: CLLocation!
    
    /// Latitude for the search.
    internal var latitude: Double {
        get {
            return self.location.coordinate.latitude
        }
    }
    
    /// Longitude for the search.
    internal var longitude: Double {
        get {
            return self.location.coordinate.longitude
        }
    }
    
    /// Record to begin search based on location, 1 based.
    internal var start: Int = 1
    
    /// Record to end search based on location, 1 based.
    internal var end: Int = 25
    
    /**
    Short init for use with default properties or setting of other properties after init.
    - parameter deviceLocation: CLLocation of device at time of search.
    - parameter location: CLLocation to base search for nearby stores.
    */
    internal init(deviceLocation: CLLocation, location: CLLocation) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
    }

    /**
    Long init for setting up the params with a single line of code.
    - parameter deviceLocation: (CLLocation) location of device at time of search.
    - parameter location: (CLLocation) location to base search for nearby stores.
    - parameter start: (Int) record to start results of search. (1 based)
    - parameter end: (Int) record to end results of search. (1 based)
    */
    internal init(deviceLocation: CLLocation, location: CLLocation, start: Int, end: Int) {
        super.init(deviceLocation: deviceLocation)
        self.location = location
        self.start = start
        self.end = end
    }
    
    /**
    Override of base paramArray() method.
    Used by IVKRequestVenues.
    */
    override internal func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kLatitude] = "\(self.latitude)" as AnyObject?
        params[self.kLongitude] = "\(self.longitude)" as AnyObject?
        params[self.kStart] = "\(self.start)" as AnyObject?
        params[self.kEnd] = "\(self.end)" as AnyObject?
        return params
    }
}
