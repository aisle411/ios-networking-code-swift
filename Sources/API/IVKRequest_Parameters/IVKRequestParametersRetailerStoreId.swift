import Foundation

internal class IVKRequestParametersRetailerStoreId: IVKObject, IVKRequestParametersProtocol {
    
    internal struct Constants {
        static let retailerStoreId = "retailer_store_id"
    }
    
    internal var retailerStoreId: Int = 0
    
    internal init(retailerStoreId: Int) {
        self.retailerStoreId = retailerStoreId
    }
    
    internal func paramArray() -> [String : AnyObject] {
        var params:[String: AnyObject] = [String: AnyObject]()
        params[Constants.retailerStoreId] = "\(self.retailerStoreId)" as AnyObject
        return params
    }
}
