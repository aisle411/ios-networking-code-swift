//
//  IVKRequestAutoCompleteParameters.swift
//  VenueKit
//
//  Created by Minxin Guo on 2/10/17.
//  Copyright © 2017 Aisle411. All rights reserved.
//

import UIKit
import CoreLocation

class IVKRequestAutoCompleteParameters: IVKRequestParameters {
    let kTerm: String = "term"
    let kVenueId: String = "retailer_store_id"

    var venueId = 0
    var searchTerm = ""
    
    init(deviceLocation: CLLocation, vendorVenueId: Int, searchTerm: String) {
        super.init(deviceLocation: deviceLocation)
        self.venueId = vendorVenueId
        self.searchTerm = searchTerm
    }
    
    /**
     Override of base paramArray() method.
     Used by IVKRequestPartnerVenue.
     */
    override func paramArray() -> [String : AnyObject] {
        var params:[String : AnyObject] = super.paramArray()
        params[self.kVenueId] = "\(self.venueId)" as AnyObject?
        params[self.kTerm] = "\(self.searchTerm)" as AnyObject?
        return params
    }
}
