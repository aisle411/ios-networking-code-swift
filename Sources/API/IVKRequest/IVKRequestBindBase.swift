import Foundation

class IVKRequestBindBase: IVKRequest {
    
    internal struct Constants {
        static let bindingsKey = "bindings"
        static let accountIdKey = "account_id"
    }
    
    //MARK: Init
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:[Constants.bindingsKey, Constants.accountIdKey], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params[Constants.accountIdKey] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Account id parameter is missing."))
            valid = false
        }
        
        if params[Constants.bindingsKey] != nil{
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Bindings parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
