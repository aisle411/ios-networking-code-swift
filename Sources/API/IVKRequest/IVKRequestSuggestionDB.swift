//
//  IVKRequestSuggestionDB.swift
//  VenueKit
//
//  Created by Fred Priese on 12/9/15.
//  Copyright © 2015 aisle411. All rights reserved.
//


import Foundation

///Search suggestions db.
internal class IVKRequestSuggestionDB: IVKRequest {
    internal let cacheRootPath: URL
    
    //  internal class let apiName: String = "getsuggestionsdb"
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getsuggestionsdb"
    }
    
    //MARK: Init
    internal convenience init?(cacheRootPath: URL) {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server, cacheRootPath: cacheRootPath)
        }
        return nil
    }
    
    internal init?(server: AisleServer, cacheRootPath: URL) {
        self.cacheRootPath = cacheRootPath
        super.init(server: server)
        self.returnsBinary = true
    }
    
    /**
     Method used for executing an IVKRequestSuggestionDB for developers who prefer using blocks.
     Use this with local caching of the db.  If not modified since passed lastModifiedDate, no db will be returned.
     - parameter lastModifiedDate: Date
     - parameter params: IVKRequestParametersProtocol
     - parameter reply: ((result: IVKResult) -> ())
     */
    internal func executeRequest(lastModifiedDate: Date, params: IVKRequestParametersProtocol, reply: @escaping ((_ result: IVKResult) -> ())) {
        self.lastModifiedDate = lastModifiedDate
        self.useLastModifiedDate = true
        super.executeRequest(params: params, reply: {
            (result: IVKResult) in
            reply(result)
        })
    }
    
    //MARK: - Class
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResult {
        let req: IVKRequestSuggestionDB = request as! IVKRequestSuggestionDB
        return IVKResultSuggestionDB(params: params, url: url, cacheRootPath: req.cacheRootPath)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDataIntoResult(responseData: Data, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultSuggestionDB = result as! IVKResultSuggestionDB
        
        workingResult.cacheSource(dbData: responseData)
        
        return workingResult
    }
    
    ///Request specific response parser for usage with lastModifiedDate.  **Used by IVKRequest**
    override class func parseNotModifiedResponseIntoResult(result: IVKResult) -> IVKResult {
        let workingResult: IVKResultSuggestionDB = result as! IVKResultSuggestionDB
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}


