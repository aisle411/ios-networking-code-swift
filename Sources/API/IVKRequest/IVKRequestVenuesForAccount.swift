import Foundation

internal class IVKRequestVenuesForAccount: IVKRequestVenues {
    //  internal class let apiName: String = "getstores"
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getuserstores"
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:[IVKVenuesForAccountRequestParameters.Constants.accountId,
                          IVKVenuesForAccountRequestParameters.Constants.start,
                          IVKVenuesForAccountRequestParameters.Constants.end], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _ = params[IVKVenuesForAccountRequestParameters.Constants.accountId] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Account id parameter is missing."))
            valid = false
        }
        
        if let _ = params[IVKVenuesForAccountRequestParameters.Constants.start] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Start parameter is missing."))
            valid = false
        }
        
        if let _ = params[IVKVenuesForAccountRequestParameters.Constants.end] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "End parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
    
    override internal class var storesKey: String {
        get {
            return "retailerStores"
        }
    }
}
