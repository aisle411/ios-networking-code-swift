//
//  IVKRequestLocateItemsWithItems.swift
//  VenueKit
//
//  Created by Fred Priese on 1/13/16.
//  Copyright (c) 2016 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestLocateItemsWithItems: IVKRequest {
//  internal class let apiName: String = "locateitemswithitems"
    
    override class func apiName() -> String {
        return "locateitemswithitems"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }
    
    //MARK: - Class
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenueItemWithVenueItems {
        return IVKResultVenueItemWithVenueItems(params: params, url: url)
    }

    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueItemWithVenueItems = result as! IVKResultVenueItemWithVenueItems
        
        var items: NSArray = NSArray()
        // VenueItems
        if let venueItems: NSArray = responseDict.object(forKey: "items") as? NSArray {
            items = venueItems
        }
        
        var i: Int = 1
        for item in items {
            if let itemDict: [String: AnyObject] = item as? [String: AnyObject] {
                let syn: [String: AnyObject] = itemDict
                var venueItems: [IVKVenueItem] = []
                if let prds: [[String: AnyObject]] = itemDict["products"] as? [[String: AnyObject]] {
                    // build venue items array
                    var j: Int = 1
                    for prd in prds {
                        let venueItem = IVKVenueItem(dict: prd, ordinal: j)
                        venueItems.append(venueItem)
                        j += 1
                    }
                }
                let synonym = IVKVenueItem(dict: syn, ordinal: i)
                let pwp: IVKVenueItemWithVenueItems = IVKVenueItemWithVenueItems(parent: synonym, children: venueItems)
                workingResult.addVenueItemWithVenueItems(venueItem: pwp)
                i += 1
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude","retailer_store_id","shopping_list_data"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["shopping_list_data"] as? String {
            // So far so good
        } else {
            if let _: [String: AnyObject] = params["shopping_list_data"] as? [String: AnyObject] {
                // So far so good
            } else {
                errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search dictionary parameter is missing."))
                valid = false                
            }
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        return (success: valid, errors: errors)
    }
}
