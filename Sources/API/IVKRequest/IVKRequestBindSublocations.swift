import Foundation

class IVKRequestBindSublocations: IVKRequestBindBase {
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "bindmapidstosublocations"
    }
}
