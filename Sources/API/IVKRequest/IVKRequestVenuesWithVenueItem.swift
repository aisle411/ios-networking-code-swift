//
//  IVKRequestVenuesWithVenueItem.swift
//  VenueKit
//
//  Created by Fred Priese on 4/9/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation
/**
`IVKRequestVenuesWithVenueItem` is used to get an array of `IVKVenueWithVenueItem`s objects in a `IVKResultVenueWithVenueItem` object.
*/
internal class IVKRequestVenuesWithVenueItem: IVKRequest {
//  internal class let apiName: String = "getstoreswiths"

    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override convenience init?(server: AisleServer) {
        self.init(server: server)
    }
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getstoreswithproduct"
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenueWithVenueItem {
        return IVKResultVenueWithVenueItem(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueWithVenueItem = result as! IVKResultVenueWithVenueItem
        
        if let venues: NSArray = responseDict.object(forKey: "stores") as? NSArray {
            var i: Int = 1
            for venue in venues {
                if let venueDict: [String: AnyObject] = venue as? [String: AnyObject] {
                    workingResult.addVenue(venue: IVKVenueWithVenueItem(dict: venueDict as [NSString : AnyObject], ordinal: i))
                    i += 1
                }
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["latitude","longitude","device_latitude","device_longitude","distance","max_stores"], hasTermUPC: true)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["term"] as? String {
            // So far so good
        } else {
            if let _: String = params["upc"] as? String {
                // So far so good
            } else {
                errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "No search term or upc was found."))
                valid = false
            }
        }
        
        if let _: String = params["latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search latitide parameter is missing."))
            valid = false
        }
        
        if let _: String = params["longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search longitude parameter is missing."))
            valid = false
        }

        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }

        if let _: String = params["distance"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Distance parameter is missing."))
            valid = false
        }
        
        if let _: String = params["max_stores"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Maximum venues parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
