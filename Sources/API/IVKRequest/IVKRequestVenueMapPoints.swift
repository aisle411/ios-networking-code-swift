//
//  IVKRequestVenueMapPoints.swift
//  IVKLive
//
//  Created by Fred Priese on 7/20/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestVenueMapPoints: IVKRequest {
    //  internal class let apiName: String = "getstoresublocations"
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getstoresublocations"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenueMapPoints {
        return IVKResultVenueMapPoints(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueMapPoints = result as! IVKResultVenueMapPoints
        
        if let dict: [String: AnyObject] = responseDict as? [String: AnyObject] {
            if let sublocs: [AnyObject] = dict["sublocations"] as? [AnyObject] {
                workingResult.addMapPoints(rawArray: sublocs)
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["retailer_store_id","device_latitude","device_longitude"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
