//
//  IVKRequestMetrics.swift
//  VenueKit
//
//  Created by Fred Priese on 3/9/16.
//  Copyright © 2016 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestMetrics: IVKRequest {
    //  internal class let apiName: String = "logmetrics"
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "logmetrics"
    }
    
    // Write Only request
    internal func submitMetric(params: IVKRequestParametersProtocol) {
        self.executeRequest(params: params, reply: {(rslt:IVKResult) -> Void in})
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude","retailer_store_id","metrics"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        let errors: [IVKError] = []
        let valid: Bool = true
        
        //TODO: add validation for params
        
        //        if let _: String = params["device_latitude"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamDeviceLatitude))
        //            valid = false
        //        }
        //
        //        if let _: String = params["device_longitude"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamDeviceLongitude))
        //            valid = false
        //        }
        
        //        if let tmp: String = params["x"] as? String {
        //            // So far so good
        //            if let tmpRealNumber: Double = Double(tmp) {
        //                if tmpRealNumber < 0 || tmpRealNumber > 1 {
        //                    errors.append(IVKError(errorType: IVKErrorType.InvalidParamX))
        //                    valid = false
        //                }
        //            }
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamX))
        //            valid = false
        //        }
        //
        //        if let tmp: String = params["y"] as? String {
        //            // So far so good
        //            if let tmpRealNumber: Double = Double(tmp) {
        //                if tmpRealNumber < 0 || tmpRealNumber > 1 {
        //                    errors.append(IVKError(errorType: IVKErrorType.InvalidParamY))
        //                    valid = false
        //                }
        //            }
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamY))
        //            valid = false
        //        }
        
        //        if let _: String = params["heading"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamHeading))
        //            valid = false
        //        }
        //
        //        if let _: String = params["accuracy"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamAccuracy))
        //            valid = false
        //        }
        //
        //        if let _: String = params["floor_level"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamFloor))
        //            valid = false
        //        }
        //
        //        if let _: String = params["retailer_store_id"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamStoreId))
        //            valid = false
        //        }
        //
        //        if let tmp: String = params["node_x"] as? String {
        //            // So far so good
        //            if let tmpRealNumber: Float = Float(tmp) {
        //                if tmpRealNumber < 0 || tmpRealNumber > 1 {
        //                    errors.append(IVKError(errorType: IVKErrorType.InvalidParamX))
        //                    valid = false
        //                }
        //            }
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamX))
        //            valid = false
        //        }
        //
        //        if let tmp: String = params["node_y"] as? String {
        //            // So far so good
        //            if let tmpRealNumber: Float = Float(tmp) {
        //                if tmpRealNumber < 0 || tmpRealNumber > 1 {
        //                    errors.append(IVKError(errorType: IVKErrorType.InvalidParamY))
        //                    valid = false
        //                }
        //            }
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.InvalidParamY))
        //            valid = false
        //        }
        //
        //        if let _: String = params["node_name"] as? String {
        //            // So far so good
        //        } else {
        //            errors.append(IVKError(errorType: IVKErrorType.generalError))
        //            valid = false
        //        }
        
        return (success: valid, errors: errors)
    }
}
