import Foundation

class IVKRequestSublocations: IVKRequest {
 
    override class func apiName() -> String {
        return "getstoresublocations"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultSublocation {
        return IVKResultSublocation(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult = result as! IVKResultSublocation
        
        if let venues: NSArray = responseDict.object(forKey: "sublocations") as? NSArray {
            for venue in venues {
                if let venueDict: [String: AnyObject] = venue as? [String: AnyObject] {
                    workingResult.add(sublocation: IVKSublocation(dict: venueDict as [NSString : AnyObject]))
                }
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["retailer_store_id"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Retailer store idparameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
