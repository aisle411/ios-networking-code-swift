import Foundation

internal class IVKRequestSignup: IVKRequestSignin {

    /// Aisle411 API name.
    override class func apiName() -> String {
        return "signup"
    }
    
}
