//
//  IVKResultAccount.swift
//  IVKLive
//
//  Created by Dante Cannarozzi on 8/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//
import Foundation

internal class IVKRequestSignin: IVKRequest {
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "signin"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }

    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultAccount {
        return IVKResultAccount(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultAccount = result as! IVKResultAccount
        
        if let accountId = responseDict.object(forKey: "account_id") as? Int {
            workingResult.accountId = accountId
        }
        
        if let venues: NSArray = responseDict.object(forKey: "venues") as? NSArray {
            var i: Int = 1
            for venue in venues {
                if let venueDict: [String: AnyObject] = venue as? [String: AnyObject] {
                    workingResult.addVenue(venue: IVKVenue(dict: venueDict as [NSString : AnyObject], ordinal: i))
                }
                i += 1
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["e-mail","password","device_latitude","device_longitude"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["e-mail"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Email parameter is missing."))
            valid = false
        }
        
        if let _: String = params["password"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "PAssword parameter is missing."))
            valid = false
        }
            
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}

