//
//  IVKRequestVenues.swift
//  VenueKit
//
//  Created by Fred Priese on 4/27/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation
/**
`IVKRequestVenues` is used to get an array of `IVKVenue`s objects in a `IVKResultVenue` object.
*/
internal class IVKRequestVenues: IVKRequest {
//  internal class let apiName: String = "getstores"
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getstores"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    internal class var storesKey: String {
        get {
            return "stores"
        }
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenue {
        return IVKResultVenue(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenue = result as! IVKResultVenue

        if let venues: NSArray = responseDict.object(forKey: self.storesKey) as? NSArray {
            var i: Int = 1
            for venue in venues {
                if let venueDict: [String: AnyObject] = venue as? [String: AnyObject] {
                    workingResult.addVenue(venue: IVKVenue(dict: venueDict as [NSString : AnyObject], ordinal: i))
                    i += 1
                }
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["latitude","longitude","device_latitude","device_longitude","start","end"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true

        if let _: String = params["latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["start"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Start parameter is missing."))
            valid = false
        }

        if let _: String = params["end"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Stop parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
