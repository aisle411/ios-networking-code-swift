//
//  IVKRequestAutoComplete.swift
//  VenueKit
//
//  Created by Minxin Guo on 2/10/17.
//  Copyright © 2017 Aisle411. All rights reserved.
//

import UIKit

class IVKRequestAutoComplete: IVKRequest {
    override class func apiName() -> String {
        return "autocomplete"
    }
    
    //MARK: Init
    convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
        
    //MARK: - Class
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultAutoComplete {
        return IVKResultAutoComplete(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult = result as! IVKResultAutoComplete
        
        var items: NSArray = NSArray()
        // venue items
        if let listItems: NSArray = responseDict.object(forKey: "specific") as? NSArray {
            items = listItems
        }
        
        if items.count > 0 {
            for (index, item) in items.enumerated() {
                if let itemDict: [String: AnyObject] = item as? [String: AnyObject] {
                    workingResult.addAutoCompleteItem(item: IVKAutoCompleteItem(dict: itemDict, ordinal: index))
                }
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["term","retailer_store_id"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["term"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "No search term was found."))
            valid = false
        }
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "retailer_store_id parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
