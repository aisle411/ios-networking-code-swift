//
//  IVKRequestLocateItems.swift
//  VenueKit
//
//  Created by Fred Priese on 4/27/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestLocateItems: IVKRequest {
//  internal class let apiName: String = "locateitems"
    
    override class func apiName() -> String {
        return "locateitems"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }
    
    //MARK: - Class
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenueItem {
        return IVKResultVenueItem(params: params, url: url)
    }

    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueItem = result as! IVKResultVenueItem
        
        var items: NSArray = NSArray()
        // venue items
        if let listItems: NSArray = responseDict.object(forKey: "items") as? NSArray {
            items = listItems
        }
        
        if items.count > 0 {
            var i: Int = 1
            for item in items {
                if let itemDict: [String: AnyObject] = item as? [String: AnyObject] {
                    workingResult.addVenueItem(venueItem: IVKVenueItem(dict: itemDict, ordinal: i))
                    i += 1
                }
            }
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude","retailer_store_id","shopping_list_data"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["shopping_list_data"] as? String {
            // So far so good
        } else {
            if let _: [String: AnyObject] = params["shopping_list_data"] as? [String: AnyObject] {
                // So far so good
            } else {
                errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search Dictionary parameter is missing."))
                valid = false                
            }
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        return (success: valid, errors: errors)
    }
}
