//
//  IVKRequest.swift
//  SKit
//
//  Created by Fred Priese on 4/3/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

#if WATCHKIT
import WatchKit
//import CryptoSwift
#else
    #if os(iOS)
    import UIKit
    #else
    import AppKit
    #endif
//import CryptoSwift
#endif

/**
`IVKRequest` is the base class for all all supported Aisle411 API requests.  Alone, it is not terribly useful.  But as a subclass, its power is great.
*/
internal class IVKRequest : IVKObject {

    //MARK: - Private Constants
//    private let credentialed: Bool
//    private let defaultAPIHost: String = "aisle411.ws"
//    private let defaultAPIVersion: Int = 3
    private let apiHost: String
//    private let apiVersion: Int
    private let partnerID: Int
    private let partnerSecret: String
    private let apiBase: String = "webservices"
    private let api: String
    private let rootCommand: String
    
    //MARK: - Internal Properties
    internal var _forcePost = false
    internal var returnsBinary: Bool = false
    internal var lastModifiedDate: Date?
    internal var useLastModifiedDate: Bool = false
    
    //MARK: - Private Properties
    private var bodyData: Data?
    private var authForPostRequest: String?
    
    /// Random GUID assigned by VenueKit to this particular device.  Stored in your application's NSUserDefaults.
    internal var guid: String {
        get {
            return self.getGUID()
        }
    }
    
    internal var useSSL: Bool = true
    private var requestProtocol: String {
        get {
            if self.useSSL {
                return "https"
            } else {
                return "http"
            }
        }
    }

    /// How long VenueKit should wait (in seconds) for a server response before failing.  Default value is 20.0.
    internal var requestTimeOut: TimeInterval = 20.0
    
    private var agent: String = ""
    /// Derived User Agent string for your application and the user's device.
    internal var userAgentString: String {
        get {
            return self.userAgent()
        }
    }
    
    // Set us up some constants!
    //MARK: - Initialization
    internal init?(server: AisleServer) {
        if !server.credentialed {
            return nil
        }
        self.apiHost = server.apiHost
        self.partnerID = server.partnerID
        self.partnerSecret = server.partnerSecret
        self.api = server.api
        self.rootCommand = type(of: self).apiName()
    }
    
    /**
    Method used for executing an IVKRequest for developers who prefer using blocks.
    - parameter params: IVKRequestParametersProtocol
    - parameter reply: ((result: IVKResult) -> ())
    */
    internal func executeRequest(params: IVKRequestParametersProtocol, reply: @escaping ((_ result: IVKResult) -> ())) {
        let paramArray: [String: AnyObject] = params.paramArray()
        let validation: (success: Bool, errors: [IVKError]) = type(of: self).validateParams(params: paramArray)
        if !validation.success {
            let result = type(of: self).newResult(params: paramArray, url: nil, request: self)
            for error in validation.errors {
                result.addError(error: error)
            }
            
            self.runAsync {
                reply(result)
            }
            return
        }
        
        self.request(params: paramArray, reply: {
            (result: IVKResult) in
            
            reply(result)
        })
    }
    
    @discardableResult internal func resetAnonymousDeviceIdentity() -> Bool {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.removeObject(forKey: AisleServer.kIVKAnonymousDeviceIdentityKey)
        //firstTry
        if let _ = defaults.string(forKey: AisleServer.kIVKAnonymousDeviceIdentityKey) {
            defaults.set("", forKey: AisleServer.kIVKAnonymousDeviceIdentityKey)
            if let secondTry: String = defaults.string(forKey: AisleServer.kIVKAnonymousDeviceIdentityKey) {
                if secondTry == "" {
                    return true
                }
            }
        }
        return true
    }
    
    //MARK: - Internal
    internal func request(params: [String: AnyObject], reply: @escaping ((_ result: IVKResult) -> ())) {
        let startTime: Date = Date()
        let urlResponse: (url: URL, error: IVKError?) = self.prepareRequest(params: params)
        if let error:IVKError = urlResponse.error {
            let result = type(of: self).newResult(params: params, url: nil, request: self)
            result.addError(error: error)

            self.runAsync {
                reply(result)
            }
            return
        }
        var result: IVKResult
        let url: URL = urlResponse.url
        
        let diagnostic: IVKRequestDiagnostic = IVKRequestDiagnostic(url: url, startTime: startTime)
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: self.requestTimeOut)
        
        if self._forcePost {
            request.httpMethod = "POST"
            request.setValue(self.authForPostRequest!, forHTTPHeaderField: "Authentication")
            request.httpBody = self.bodyData!
        } else {
            request.httpMethod = "GET"
        }
        request.setValue(self.userAgentString, forHTTPHeaderField: "User-Agent")
        if self.useLastModifiedDate {
            if let lMD: Date = self.lastModifiedDate {
                let formatter: DateFormatter = DateFormatter()
                formatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
                formatter.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
                let dateString: String = formatter.string(from: lMD)
                babelfish.verbose("Last Modified Date of cached data: \(dateString) [\(type(of: self).apiName())]")
                request.setValue(dateString, forHTTPHeaderField: "If-Modified-Since")
            }
        }
        
        // get IVKResult sub class from IVKRequest subclass
        result = type(of: self).newResult(params: params, url: url, request: self)
        
        diagnostic.beginNetwork()
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let httpResponse: HTTPURLResponse = response as? HTTPURLResponse {
                var returnVal: AnyObject?
                if httpResponse.statusCode == 200 {
                    if let responseData = data {
                        let d: Data = responseData as Data
                        diagnostic.processResponse(response: httpResponse, byteSize: d.count)
                        if self.returnsBinary {
                            result = type(of: self).parseResponseDataIntoResult(responseData: responseData, result: result)
                        } else {
                            do {
                                try returnVal = JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                if let resultDict: NSDictionary = returnVal as? NSDictionary {
                                    if let fault: [String: AnyObject] = resultDict.object(forKey: "Fault") as? [String: AnyObject] {
                                        result = type(of: self).newResult(params: params, url: httpResponse.url, request: self)
                                        var error: IVKError
                                        if let code = fault["Code"] as? Int {
                                            if code == 1 || code == 2 {
                                                error = IVKError(errorType: IVKErrorType.authenticationFailed)
                                            } else {
                                                error = IVKError(errorNumber: 20000+code, errorMessage: fault["Description"] as! String)
                                            }
                                        } else {
                                            error = IVKError(errorType: IVKErrorType.generalError)
                                        }
                                        
                                        result.addError(error: error)
                                    } else  {
                                        result = type(of: self).parseResponseDictionaryIntoResult(responseDict: resultDict, result: result)
                                    }
                                } else {
                                    if let resultArray: NSArray = returnVal as? NSArray {
                                        result = type(of: self).parseResponseArrayIntoResult(responseArray: resultArray, result: result)
                                    } else {
                                        let rawText: String = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue) as! String
                                        result = type(of: self).parseResponseStringIntoResult(responseString: rawText, result: result)
                                    }
                                }
                            } catch {
                                result = type(of: self).newResult(params: params, url: httpResponse.url, request: self)
                                result.addError(error: IVKError(errorType: IVKErrorType.generalError))
                            }
                        }
                    } else {
                        diagnostic.processResponse(response: httpResponse, byteSize: 0)
                        
                        result.addError(error: IVKError(errorType: IVKErrorType.emptyResponse))
                        
                        if let theError: NSError = error as NSError? {
                            // add error info to the result
                            let asyncError: IVKError = IVKError(errorNumber: theError.code, errorMessage: theError.localizedDescription)
                            result.addError(error: asyncError)
                        }
                    }
                } else if httpResponse.statusCode == 304 {
                    diagnostic.processResponse(response: httpResponse, byteSize: 0)
                    result = type(of: self).parseNotModifiedResponseIntoResult(result: result)
                } else {
                    diagnostic.processResponse(response: httpResponse, byteSize: 0)
                    
                    result.addError(error: IVKError(errorNumber: httpResponse.statusCode, errorMessage: httpResponse.description))
                    
                    if let theError: NSError = error as NSError? {
                        // add error info to the result
                        let asyncError: IVKError = IVKError(errorNumber: theError.code, errorMessage: theError.localizedDescription)
                        result.addError(error: asyncError)
                    }
                }
            } else {
                // Whoa, big trouble.
                diagnostic.processResponse(response: HTTPURLResponse(), byteSize: 0)
                
                result.addError(error: IVKError(errorType: IVKErrorType.httpRequestError))
                
                if let theError: NSError = error as NSError? {
                    // add error info to the result
                    let asyncError: IVKError = IVKError(errorNumber: theError.code, errorMessage: theError.localizedDescription)
                    result.addError(error: asyncError)
                }
            }
            
            diagnostic.endDiagnostic()
            result.attachDiagnostic(diagnostic: diagnostic)
            reply(result)
        })
        task.resume()
    }
  
    //MARK: Class Methods (to be overridden)
    /// Meant to be overridden.
    class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResult {
        return IVKResult(params: params, url: url)
    }
    
    /// Meant to be overridden.
    class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        return IVKResult(params: [:], url: nil)
    }
    
    /// Meant to be overridden.
    class func parseResponseArrayIntoResult(responseArray: NSArray, result: IVKResult) -> IVKResult {
        return IVKResult(params: [:], url: nil)
    }
    
    /// Meant to be overridden.
    class func parseResponseStringIntoResult(responseString: String, result: IVKResult) -> IVKResult {
        return IVKResult(params: [:], url: nil)
    }
    
    /// Meant to be overridden.
    class func parseResponseDataIntoResult(responseData: Data, result: IVKResult) -> IVKResult {
        return IVKResult(params: [:], url: nil)
    }

    /// Meant to be overridden.
    class func parseNotModifiedResponseIntoResult(result: IVKResult) -> IVKResult {
        return IVKResult(params: [:], url: nil)
    }
    
    /// Meant to be overridden.
    class func apiName() -> String {
        return ""
    }
    
    /// Meant to be overridden.
    class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:[], hasTermUPC: false)
    }
    
    /// Meant to be overridden.
    class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        return (success: false, errors: [IVKError(errorType: IVKErrorType.generalError)])
    }
    
    //MARK: - Private
    private func prepareRequest(params: [String: AnyObject]) -> (url: URL, error: IVKError?) {
        var urlResponse: (url:URL, error:IVKError?)
        if self._forcePost {
            urlResponse = self.urlForPostRequest(params: params)
        } else {
            urlResponse = self.urlForGetRequest(params: params)
        }
        
        return urlResponse
    }
    
    private func urlForGetRequest(params: [String: AnyObject]) -> (url: URL, error: IVKError?) {
       let request: String = self.addGetAuth(params: params)
        if request == "MISSING REQUIRED PARAMS" {
            return (url: URL(string: "")!, error:IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Required parameters missing."))
        }
        
        let url: String = "\(self.requestProtocol)://\(self.apiHost)/\(self.api)/\(request)"
        babelfish.info("URL(GET): \(url)")
        if let URL: URL = URL(string: url) {
            return (url: URL, nil)
        } else {
            return (url: URL(string: "")!, error:IVKError(errorType: IVKErrorType.generalError))
        }
        
    }
  
    //FIXME: add support for passing along all params, not just required.  Don't have time to validate that this won't break anything.
    private func urlForPostRequest(params: [String: AnyObject]) -> (url: URL, error: IVKError?) {
        // filter required params out of passed params, fail if we don't find all of them.
        var rParams: IVKAuthParams = IVKAuthParams()
//        var aParams: IVKAuthParams = IVKAuthParams()
        
        var requiredParams: [String: AnyObject] = [:]
        let requiredKeys: (required: [String], hasTermUPC: Bool) = type(of: self).requiredParams()
        
        func isRequired(key: String) -> Bool {
            for k in requiredKeys.required {
                if key == k {
                    return true
                } else {
                    if requiredKeys.hasTermUPC {
                        if key == "term" || key == "upc" {
                            return true
                        }
                    }
                }
            }
            return false
        }
        
        var rKModifier: Int = 0
        if requiredKeys.hasTermUPC {
            rKModifier = 1
        }
        for (k, v) in params {
            var ap: IVKAuthParam
            if v is [String: Any] {
                ap = IVKAuthParam(key: k, dictionary: v as! [String: Any])
            } else if v is [[String:AnyObject]] {
                ap = IVKAuthParam(key: k, array: v as! [[String:Any]])
            } else {
                ap = IVKAuthParam(key: k, value: "\(v)")
            }

            if isRequired(key: k) {
                rParams.params.append(ap)
            }
        }

        if rParams.params.count != requiredKeys.required.count + rKModifier {
            return (url: URL(string: "")!, error:IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Required parameters missing."))
        }
        
//        aParams.params.append(IVKAuthParam(key: "device_token", value: self.getGUID()))
        rParams.params.append(IVKAuthParam(key: "device_token", value: self.getGUID()))
        
//        aParams.params.append(IVKAuthParam(key: "partner_id", value: "\(self.partnerID)"))
        rParams.params.append(IVKAuthParam(key: "partner_id", value: "\(self.partnerID)"))
        
        let urlForRequest: String = "\(self.rootCommand).php"
        
        var authBody: String = rParams.authStringForHTTPPost()
        authBody += self.partnerSecret
        self.bodyData = rParams.dataForHTTPPost()
        self.authForPostRequest = authBody.md5().lowercased()
        
        let url: NSString = "\(self.requestProtocol)://\(self.apiHost)/\(self.api)/\(urlForRequest)" as NSString
        
        babelfish.info("URL(POST): \(url)")

//        aParams.params.append(IVKAuthParam(key: "url", value: url as String))
//        aParams.params.append(IVKAuthParam(key: "httpHeader_Authentication", value: self.authForPostRequest!))
        
        return (url: URL(string: url as String)!, error: nil)
    }
    
    private func getGUID() -> String {
        var defaults: UserDefaults = UserDefaults.standard
        var result: String = ""
        let infoDict = Bundle.main.infoDictionary!
        if let groupKey: String = infoDict[AisleServer.kDefaultsGroupKey] as? String {
            if let groupDefaults = UserDefaults(suiteName: groupKey) {
                defaults = groupDefaults
            }
        }
        
        if let defRslt: String = defaults.string(forKey: AisleServer.kIVKAnonymousDeviceIdentityKey) {
            if defRslt == "" {
                result = NSUUID().uuidString
                defaults.set(result, forKey: AisleServer.kIVKAnonymousDeviceIdentityKey)
            }
            result = defRslt;
        } else {
            result = NSUUID().uuidString
            defaults.set(result, forKey: AisleServer.kIVKAnonymousDeviceIdentityKey)
        }
        return result.sha1().lowercased()
    }
    
    private func addGetAuth(params: [String: AnyObject]) -> String {
        // filter required params out of passed params, fail if we don't find all of them.
        var rParams: IVKAuthParams = IVKAuthParams()
        var aParams: IVKAuthParams = IVKAuthParams()
        
        var requiredParams: [String: AnyObject] = [:]
        let requiredKeys: (required: [String], hasTermUPC: Bool) = type(of: self).requiredParams()
        
        func isRequired(key: String) -> Bool {
            for k in requiredKeys.required {
                if key == k {
                    return true
                } else {
                    if requiredKeys.hasTermUPC {
                        if key == "term" || key == "upc" {
                            return true
                        }
                    }
                }
            }
            return false
        }
        
        var rKModifier: Int = 0
        if requiredKeys.hasTermUPC {
            rKModifier = 1
        }
        for (k, v) in params {
            let ap = IVKAuthParam(key: k, value: v as! String)
            aParams.params.append(ap)
            if isRequired(key: k) {
                rParams.params.append(ap)
            }
        }
        
        if rParams.params.count != requiredKeys.required.count + rKModifier {
            return "MISSING REQUIRED PARAMS"
        }
        
        aParams.params.append(IVKAuthParam(key: "device_token", value: self.getGUID()))
        aParams.params.append(IVKAuthParam(key: "partner_id", value: "\(self.partnerID)"))

        // set us up some urls
        var urlForHashing: String = self.rootCommand
        var urlForRequest: String = "\(self.rootCommand).php"
        
        urlForHashing += aParams.queryString(encode: false)
        urlForRequest += aParams.queryString(encode: true)

        // compute hash for authentication using url for authentication
        // and then append it to the url for the request
        urlForHashing += "&" + self.partnerSecret

        let hash = urlForHashing.md5().lowercased()
        urlForRequest += "&auth=\(hash)"

        return urlForRequest
    }
    
    private func userAgent() -> String {
        if self.agent == "" {
            let infoDict: [NSObject: AnyObject] = Bundle.main.infoDictionary! as [NSObject : AnyObject]
            if let appName: String = infoDict["CFBundleName" as NSObject] as? String {
                if let appVersion: String = infoDict["CFBundleVersion" as NSObject] as? String {
                    var name: String = self.stringReplace(source: appName, target: " ", withString: "-")
                    name = self.stringReplace(source: name, target: "_", withString: "=")
                    var version: String = self.stringReplace(source: appVersion, target: " ", withString: "-")
                    version = self.stringReplace(source: version, target: "_", withString: "=")
                    #if WATCHKIT
                    var osVersion = "WatchOS(\(WKInterfaceDevice.currentDevice().systemVersion))"
                    #else
                    var osVersion = "iOS(\(UIDevice.current.systemVersion))"
                    #endif
                    osVersion = self.stringReplace(source: osVersion, target: " ", withString: "-")
                    osVersion = self.stringReplace(source: osVersion, target: "_", withString: "=")
                    var frameworkNameAndVersion = "VenueKit-1.0.0"
                    if let ivkBundle: Bundle = self.findMyFrameworkBundle() {
                        let infoPlist: [NSObject: AnyObject] = ivkBundle.infoDictionary! as [NSObject : AnyObject]
                        frameworkNameAndVersion = (infoPlist["CFBundleName" as NSObject] as! String) + "-" + (infoPlist["CFBundleShortVersionString" as NSObject] as! String)
                    }
                    
                    self.agent = "\(name)_\(version)(\(frameworkNameAndVersion))_\(osVersion)"
                }
            }
        }
        return self.agent
    }
    
    private func stringReplace(source: String, target: String, withString: String) -> String
    {
        return source.replacingOccurrences(of: target, with: withString, options: .literal, range: nil) 
    }
    
    private func findMyFrameworkBundle() -> Bundle? {
        let frameworkBundles = Bundle.allFrameworks
        if frameworkBundles.count > 0 {
            for bundle in frameworkBundles {
                if let identifier = bundle.bundleIdentifier {
                    if identifier == AisleServer.kVenueKitId {
                        return bundle
                    }
                }
            }
        }
        return nil
    }
    
//    deinit {
//        babelfish.severe("IVKRequest is being released.")
//    }
}
