//
//  IVKRequestVenueRasterMap.swift
//  VenueKit
//
//  Created by Fred Priese on 4/27/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

///Legacy Aisle411 Raster Map request.
internal class IVKRequestVenueRasterMap: IVKRequest {
//  internal class let apiName: String = "map"
    internal let cacheRootPath: URL
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "map"
    }
    
    //MARK: Init
    internal convenience init?(cacheRootPath: URL) {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server, cacheRootPath: cacheRootPath)
        }
        return nil
    }
    
    internal init?(server: AisleServer, cacheRootPath: URL) {
        self.cacheRootPath = cacheRootPath
        super.init(server: server)
        self.returnsBinary = true
    }
    
    /**
    Method used for executing an IVKRequestVenueRasterMap for developers who prefer using blocks.
    Use this with local caching of the map.  If not modified since passed lastModifiedDate, no map will be returned.
    - parameter lastModifiedDate: Date
    - parameter params: IVKRequestParametersProtocol
    - parameter reply: ((result: IVKResult) -> ())
    */
    internal func executeRequest(lastModifiedDate: Date, params: IVKRequestParametersProtocol, reply: @escaping ((_ result: IVKResult) -> ())) {
        self.lastModifiedDate = lastModifiedDate
        self.useLastModifiedDate = true
        super.executeRequest(params: params, reply: {
            (result: IVKResult) in
            reply(result)
        })
    }
    
    //MARK: - Class
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResult {
        let req = request as! IVKRequestVenueRasterMap
        return IVKResultVenueRasterMap(params: params, url: url!, cacheRootPath: req.cacheRootPath)
    }

    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDataIntoResult(responseData: Data, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueRasterMap = result as! IVKResultVenueRasterMap
        
        workingResult.cacheSource(source: responseData)
                
        return workingResult
    }

    ///Request specific response parser for usage with lastModifiedDate.  **Used by IVKRequest**
    override class func parseNotModifiedResponseIntoResult(result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueRasterMap = result as! IVKResultVenueRasterMap
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["latitude","longitude", "device_latitude","device_longitude","retailer_store_id"], hasTermUPC: false)
    }

    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        return (success: valid, errors: errors)
    }
}

