import Foundation

class IVKRequestBindBeacons: IVKRequestBindBase {
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "bindmapidstobeacons"
    }
}
