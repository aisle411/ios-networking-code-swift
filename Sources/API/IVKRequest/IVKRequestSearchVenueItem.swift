//
//  IVKRequestSearchVenueItem.swift
//  VenueKit
//
//  Created by Fred Priese on 4/24/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestSearchVenueItem: IVKRequest {
//  internal class let apiName: String = "searchproduct"
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "searchproduct"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenueItem {
        return IVKResultVenueItem(params: params, url: url)
    }

    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenueItem = result as! IVKResultVenueItem
        //TODO: move all parsing into objects, use factory pattern if needed
        
        var items: [[String: AnyObject]] = []
        // venue items
        if let _venueItems = responseDict.object(forKey: "products") {
            if let venueItems: [[String: AnyObject]] = _venueItems as? [[String: AnyObject]] {
                // Is an array, hooray
                items = venueItems
            } else {
                if let item: [String: AnyObject] = _venueItems as? [String: AnyObject] {
                    // Is a dict, hooray, make it an array!
                    items = [item]
                } else {
                    // who knows what it is?!, make it empty
                    items = []
                }
            }
        } else {
            if let synonyms: [[String: AnyObject]] = responseDict.object(forKey: "product_suggestions") as? [[String: AnyObject]] {
                items = synonyms
            }
        }
        
        var i: Int = 1
        for item in items {
            workingResult.addVenueItem(venueItem: IVKVenueItem(dict: item, ordinal: i))
            i += 1
        }
        
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude","start","end", "retailer_store_id", "max_locations"], hasTermUPC: true)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["term"] as? String {
            // So far so good
        } else {
            if let _: String = params["upc"] as? String {
                // So far so good
            } else {
                errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Search term or upc not found."))
                valid = false
            }
        }
               
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["start"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Start parameter is missing."))
            valid = false
        }
        
        if let _: String = params["end"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "End parameter is missing."))
            valid = false
        }
                
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        return (success: valid, errors: errors)
    }
}
