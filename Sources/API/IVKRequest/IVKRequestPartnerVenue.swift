//
//  IVKRequestPartnerVenue.swift
//  VenueKit
//
//  Created by Fred Priese on 4/27/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestPartnerVenue: IVKRequest {
//  internal class let apiName: String = "getpartnerstore"
    
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "getpartnerstore"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultVenue {
        return IVKResultVenue(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResult {
        let workingResult: IVKResultVenue = result as! IVKResultVenue
        
        if let venues: NSArray = responseDict.object(forKey: "stores") as? NSArray {
            var i: Int = 1
            for venue in venues {
                if let venueDict: [String: AnyObject] = venue as? [String: AnyObject] {
                    workingResult.addVenue(venue: IVKVenue(dict: venueDict as [NSString : AnyObject], ordinal: i))
                }
                i += 1
            }
        }
        
        return workingResult
    }

    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["vendor_store_nbr","device_latitude","device_longitude"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["vendor_store_nbr"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Partner Venue Id parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}

