//
//  IVKRequestConvertCoordinate.swift
//  VenueKit
//
//  Created by Minxin Guo on 11/3/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

class IVKRequestConvertCoordinate: IVKRequest {
    /// Aisle411 API name.
    override class func apiName() -> String {
        return "coordinatetranslation"
    }
    
    //MARK: Init
    internal convenience init?() {
        if let ivk: AisleServer = AisleServer.shared {
            self.init(server: ivk)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
    }
    
    ///Factory type method for getting an appropriate IVKResult object.  **Used by IVKRequest**
    override class func newResult(params: [String: AnyObject], url: URL?, request: IVKRequest) -> IVKResultConvertedCoordinate {
        return IVKResultConvertedCoordinate(params: params, url: url)
    }
    
    ///Request specific response parser.  **Used by IVKRequest**
    override class func parseResponseDictionaryIntoResult(responseDict: NSDictionary, result: IVKResult) -> IVKResultConvertedCoordinate {
        let workingResult = result as! IVKResultConvertedCoordinate
        
        if let point = responseDict.object(forKey: "point") as? [String: AnyObject] {
            let x = point["x"] as! Float
            let y = point["y"] as! Float
            let floor = point["floor"] as! Int
            workingResult.addPoint(point: IVKConvertedPoint(x: x, y: y, floor: floor))
        }
        return workingResult
    }
    
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["x", "y", "retailer_store_id"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["x"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "X parameter is missing."))
            valid = false
        }
        
        if let _: String = params["y"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Y parameter is missing."))
            valid = false
        }
        
        if let _: String = params["retailer_store_id"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "VenueId parameter is missing."))
            valid = false
        }
        
        return (success: valid, errors: errors)
    }
}
