//
//  IVKRequestSendMessage.swift
//  VenueKit
//
//  Created by Fred Priese on 1/15/16.
//  Copyright (c) 2016 aisle411. All rights reserved.
//

import Foundation

internal class IVKRequestSendMessage: IVKRequest {
//  internal class let apiName: String = "sendmessage"
    
    override class func apiName() -> String {
        return "sendmessage"
    }
    
    // Write Only request
    internal func submitMessage(params: IVKRequestParametersProtocol) {
        self.executeRequest(params: params, reply: {(rslt:IVKResult) -> Void in})
    }
    
    //MARK: Init
    internal convenience init?() {
        if let server: AisleServer = AisleServer.shared {
            self.init(server: server)
        }
        return nil
    }
    
    internal override init?(server: AisleServer) {
        super.init(server: server)
        self._forcePost = true
    }
    
    //MARK: - Class
    ///Used for validation and authentication.  **Used by IVKRequest**
    override class func requiredParams() -> (required:[String], hasTermUPC: Bool) {
        return (required:["device_latitude","device_longitude","message"], hasTermUPC: false)
    }
    
    ///Determines parameter validity.  **Used by IVKRequest**
    override class func validateParams(params: [String: AnyObject]) -> (success: Bool, errors: [IVKError]) {
        var errors: [IVKError] = []
        var valid: Bool = true
        
        if let _: String = params["message"] as? String {
            // So far so good
        } else {
            if let _: [String: AnyObject] = params["message"] as? [String: AnyObject] {
                // So far so good
            } else {
                errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Message (dictionary) parameter is missing."))
                valid = false                
            }
        }
        
        if let _: String = params["device_latitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Latitude parameter is missing."))
            valid = false
        }
        
        if let _: String = params["device_longitude"] as? String {
            // So far so good
        } else {
            errors.append(IVKError(errorType: IVKErrorType.invalidParameter, additionalInfo: "Device Longitude parameter is missing."))
            valid = false
        }
        return (success: valid, errors: errors)
    }
}
