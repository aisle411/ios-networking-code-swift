//
//  IVKResultConvertedCoordinate.swift
//  VenueKit
//
//  Created by Minxin Guo on 11/3/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import UIKit

/**
 Class for results from IVKRequestCoordianteConversion objects.
 */
class IVKResultConvertedCoordinate: IVKResult {
    private(set) var points = [IVKConvertedPoint]()

    func addPoint(point: IVKConvertedPoint) {
        points.append(point)
    }
    
    override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}
