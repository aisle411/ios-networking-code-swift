//
//  IVKResultVenueRasterMap.swift
//  VenueKit
//
//  Created by Fred Priese on 9/23/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation

/**
Class for results from IVKRequestVenueRasterMap objects.
*/
internal class IVKResultVenueRasterMap: IVKResult {
    internal var cachedMapPath: URL
    
    /// Whether to use local cache.  (If API returns that there is no change since the lastModifiedDate provided, this will be true.)
    internal var useCache: Bool {
        get {
            return self._notModified
        }
    }

    internal func cacheSource(source: Data) {
        if self.makeDir(url: self.cachedMapPath.deletingLastPathComponent()) {
            try! source.write(to: self.cachedMapPath, options: .atomic)
        }
    }

    internal init(params: [String: AnyObject], url: URL, cacheRootPath: URL) {
        let venueId: Int = (params["retailer_store_id"] as! NSString).integerValue
        self.cachedMapPath = cacheRootPath.appendingPathComponent("\(venueId).imap")
        super.init(params: params, url: url)
    }
}
