//
//  IVKResult.swift
//  VenueKit
//
//  Created by Fred Priese on 6/1/15.
//  Copyright (c) 2015 aisle411. All rights reserved.
//

import Foundation

/**
Base class for results from IVKRequest objects.
*/
internal class IVKResult: IVKObject {
    internal var _items: [AnyObject] = []
    private var _errors: [IVKError] = []
    private var _params: [String: AnyObject] = [:]
    private var _url: URL?
    private var _diagnostic: IVKRequestDiagnostic?
    internal var _notModified: Bool = false
    private var _lastModifiedDate: Date?
    
    /// Array of IVKError objects.
    internal var errors: [IVKError] {
        get {
            return self._errors
        }
    }
    
    /// Array of params produced by IVKRequestParameters object.
    internal var params: [String: AnyObject] {
        get {
            return self._params
        }
    }
    
    /// Whether this object contains any errors.
    internal var hasErrors: Bool {
        get {
            return self._errors.count > 0
        }
    }
    
    /// URL of API request.
    internal var url: URL? {
        get {
            return self._url
        }
    }
    
    /// IVKRequestDiagnostic object.
    internal var diagnostic: IVKRequestDiagnostic? {
        get {
            return self._diagnostic
        }
    }
    
    /// Whether the API data has been modified since the supplied lstModifiedDate.
    internal var notModified: Bool {
        get {
            return self._notModified
        }
    }
    
    /// Date of last modification of this result data, if applicable.  Mainly used for IVKRequestVenueRasterMap.
    internal var lastModifiedDate: Date? {
        get {
            return self._lastModifiedDate
        }
    }
    
    internal init(params: [String: AnyObject], url: URL?) {
        self._params = params
        self._url = url
    }
    
    internal func addError(error: IVKError) {
        self._errors.append(error)
    }
    
    internal func addItem(item: AnyObject) {
        self._items.append(item)
    }
    
    internal func attachDiagnostic(diagnostic: IVKRequestDiagnostic) {
        if let _: IVKRequestDiagnostic = self._diagnostic {
        } else {
            self._diagnostic = diagnostic
            self._notModified = self._diagnostic?.httpStatus == 304
            if let lMD: Date = self._diagnostic?.lastModifiedDate {
                // We add one second to the last modified date.  Testing bears out the need for this.
                self._lastModifiedDate = lMD.addingTimeInterval(1.0)
            }
        }
    }
    
    deinit {
        self._errors = []
        self._params = [:]
        self._url = nil
        self._diagnostic = nil
        self._lastModifiedDate = nil
        
//        babelfish.severe("IVKResult is being released.")
    }
}
