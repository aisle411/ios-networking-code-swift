//
//  IVKResultAutoComplete.swift
//  VenueKit
//
//  Created by Minxin Guo on 2/10/17.
//  Copyright © 2017 Aisle411. All rights reserved.
//

import UIKit

class IVKResultAutoComplete: IVKResult {
    private var _autoCompleteItems: [IVKAutoCompleteItem] = []
    /// Array of IVKVenueItem objects.
    var autoCompleteItems: [IVKAutoCompleteItem] {
        get {
            if self._items.count > self._autoCompleteItems.count {
                self._autoCompleteItems = self._items as! [IVKAutoCompleteItem]
                self._items = []
            }
            return self._autoCompleteItems
        }
    }
    
    func addAutoCompleteItem(item: IVKAutoCompleteItem) {
        self._items.append(item)
    }
    
    override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }}
