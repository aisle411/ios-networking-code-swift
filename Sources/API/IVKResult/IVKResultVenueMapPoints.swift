//
//  IVKResultVenueMapPoints.swift
//  IVKLive
//
//  Created by Fred Priese on 7/20/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKResultVenueMapPoints: IVKResult {
    private var _namedMapPoints: [String: Int] = [:]
    
    internal var namedMapPoints: [String: Int] {
        get {
            if self._items.count > self._namedMapPoints.count {
                for item in self._items {
                    if let iDict: [String: AnyObject] = item as? [String: AnyObject] {
                        if let value: Int = iDict["value"] as? Int {
                            self._namedMapPoints[iDict["key"] as! String] = value
                        }
                    }
                }
                self._items = []
            }
            return self._namedMapPoints
        }
    }
    
    internal func addMapPoints(rawArray: [AnyObject]) {
        for item in rawArray {
            if let subloc: [String: AnyObject] = item as? [String: AnyObject] {
                if let mapIds: [Int] = subloc["map_ids"] as? [Int] {
                    if mapIds.count > 0 {
                        if let key: String = subloc["sub_location_cd"] as? String {
                            self._items.append(["key":key, "value": mapIds.first!] as AnyObject)
                        }
                    }
                }
            }
        }
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}
