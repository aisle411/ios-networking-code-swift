//
//  IVKResultSuggestionDB.swift
//  VenueKit
//
//  Created by Fred Priese on 12/9/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation

/**
 Class for results from IVKRequestSuggestionDB objects.
 */
internal class IVKResultSuggestionDB: IVKResult {
    internal let cachedDBPath: URL
    
    /// Whether to use local cache.  (If API returns that there is no change since the lastModifiedDate provided, this will be true.)
    internal var useCache: Bool {
        get {
            return self._notModified
        }
    }
    
    internal func cacheSource(dbData: Data) {
        if self.makeDir(url: self.cachedDBPath.deletingLastPathComponent()) {
            try! dbData.write(to: self.cachedDBPath, options: .atomic)
        }
    }
    
    internal init(params: [String: AnyObject], url: URL?, cacheRootPath: URL) {
        self.cachedDBPath = cacheRootPath.appendingPathComponent("suggest.sqlite")
        super.init(params: params, url: url)
    }
}
