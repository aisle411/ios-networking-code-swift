import Foundation

internal class IVKResultSublocation: IVKResult {
 
    internal var sublocations: [IVKSublocation] {
        get {
            return self._items as! [IVKSublocation]
        }
    }
    
    internal func add(sublocation: IVKSublocation) {
        self._items.append(sublocation)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }

}
