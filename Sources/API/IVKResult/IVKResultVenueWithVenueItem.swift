//
//  IVKResultVenueWithVenueItem.swift
//  VenueKit
//
//  Created by Fred Priese on 9/23/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation

/**
Class for results from IVKRequestVenuesWithVenueItem objects.
*/
internal class IVKResultVenueWithVenueItem: IVKResult {
    private var _venues: [IVKVenueWithVenueItem] = []
    /// Array of IVKVenueWithVenueItem objects.
    internal var venues: [IVKVenueWithVenueItem] {
        get {
            if self._items.count > self._venues.count {
                self._venues = self._items as! [IVKVenueWithVenueItem]
                self._items = []
            }
            return self._venues
        }
    }
    
    internal func addVenue(venue: IVKVenueWithVenueItem) {
        self._items.append(venue)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}
