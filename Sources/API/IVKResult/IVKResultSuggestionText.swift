//
//  IVKResultSuggestionText.swift
//  VenueKit
//
//  Created by Fred Priese on 9/19/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/**
 Class for results from IVKRequestSuggestionText objects.
 */
internal  class IVKResultSuggestionText: IVKResult {
    internal let cachedFilePath: URL
    private var source: [[String: AnyObject]]?
    
    internal var suggestions: IVKSuggestions? {
        get {
            if self.useCache || FileManager.default.fileExists(atPath: self.cachedFilePath.path) || self.source != nil {
                if let source = self.source {
                    return IVKSuggestions(array: source)
                } else {
                    if let source: NSArray = NSArray(contentsOf: self.cachedFilePath) {
                        if let array: [[String: AnyObject]] = source as? [[String: AnyObject]] {
                            return IVKSuggestions(array: array)
                        }
                    }
                }
            }
            return nil
        }
    }
    
    /// Whether to use local cache.  (If API returns that there is no change since the lastModifiedDate provided, this will be true.)
    internal var useCache: Bool {
        get {
            return self._notModified
        }
    }
    
    internal func cacheSource(array: [[String: AnyObject]]) {
        if self.makeDir(url: self.cachedFilePath.deletingLastPathComponent()) {
            let nsArray: NSArray = array as NSArray
            nsArray.write(toFile: self.cachedFilePath.path, atomically: true)
        }
    }
    
    internal init(params: [String: AnyObject], url: URL?, cacheRootPath: URL) {
        let venueId: Int = (params["retailer_store_id"] as! NSString).integerValue
        self.cachedFilePath = cacheRootPath.appendingPathComponent("\(venueId)")
        super.init(params: params, url: url)
    }
}
