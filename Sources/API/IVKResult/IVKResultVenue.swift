//
//  IVKResultVenue.swift
//  VenueKit
//
//  Created by Fred Priese on 9/23/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation

/**
Class for results from IVKRequestVenues objects.
*/
internal class IVKResultVenue: IVKResult {
    private var _venues: [IVKVenue] = []
    /// Array of IVKVenue objects.
    internal var venues: [IVKVenue] {
        get {
            if self._items.count > self._venues.count {
                self._venues = self._items as! [IVKVenue]
                self._items = []
            }
            return self._venues
        }
    }
    
    internal func addVenue(venue: IVKVenue) {
        self._items.append(venue)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}

