//
//  IVKResultVenueItemWithVenueItems
//  VenueKit
//
//  Created by Fred Priese on 1/13/16.
//  Copyright © 2016 aisle411. All rights reserved.
//

import Foundation

/**
Class for results from IVKRequestSearchVenueItem and IVKRequestLocateItems objects.
*/
internal class IVKResultVenueItemWithVenueItems: IVKResult {
    private var _venueItems: [IVKVenueItemWithVenueItems] = []
    
    /// Array of ISKVenueItem objects.
    internal var venueItems: [IVKVenueItemWithVenueItems] {
        get {
            if self._items.count > self._venueItems.count {
                self._venueItems = self._items as! [IVKVenueItemWithVenueItems]
                self._items = []
            }
            return self._venueItems
        }
    }
    
    internal func addVenueItemWithVenueItems(venueItem: IVKVenueItemWithVenueItems) {
        self._items.append(venueItem)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}
