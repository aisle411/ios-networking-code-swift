//
//  IVKResultAccount.swift
//  IVKLive
//
//  Created by Dante Cannarozzi on 8/18/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

/**
 Class for results from IVKRequestVenues objects.
 */
internal class IVKResultAccount: IVKResult {
    internal var accountId: Int = 0
    private var _venues: [IVKVenue] = []
    /// Array of IVKVenue objects.
    internal var venues: [IVKVenue] {
        get {
            return self._venues
        }
    }
    
    internal func addVenue(venue: IVKVenue) {
        self._venues.append(venue)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}

