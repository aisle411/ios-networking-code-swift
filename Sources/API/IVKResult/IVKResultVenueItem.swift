//
//  IVKResultVenueItem.swift
//  VenueKit
//
//  Created by Fred Priese on 9/23/15.
//  Copyright © 2015 aisle411. All rights reserved.
//

import Foundation

/**
Class for results from IVKRequestSearchVenueItem and IVKRequestLocateItems objects.
*/
internal class IVKResultVenueItem: IVKResult {
    private var _venueItems: [IVKVenueItem] = []
    /// Array of IVKVenueItem objects.
    internal var venueItems: [IVKVenueItem] {
        get {
            if self._items.count > self._venueItems.count {
                self._venueItems = self._items as! [IVKVenueItem]
                self._items = []
            }
            return self._venueItems
        }
    }
    
    internal func addVenueItem(venueItem: IVKVenueItem) {
        self._items.append(venueItem)
    }
    
    internal override init(params: [String: AnyObject], url: URL?) {
        super.init(params: params, url: url)
    }
}
