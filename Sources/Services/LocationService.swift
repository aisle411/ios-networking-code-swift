import Foundation
import CoreLocation

internal class LocationService: NSObject, CLLocationManagerDelegate {
    
    private let clManager: CLLocationManager = CLLocationManager()
    internal private(set) var deviceLocation: CLLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    internal private(set) var isReady: Bool = false
    
    internal func start() {
        self.clManager.delegate = self
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus != .authorizedWhenInUse && authStatus != .authorizedAlways {
            self.clManager.requestWhenInUseAuthorization()
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            self.clManager.startUpdatingLocation()
        }
    }
    
    /// CLLocationManagerDelegate Method
    /// ** DO NOT USE **
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            self.deviceLocation = locations.first!
            if !self.isReady {
                let note: Notification = Notification(name: Notification.Name(rawValue: AisleServer.kIVKReadyNotificationName), object: nil)
                NotificationCenter.default.post(note as Notification)
                self.isReady = true
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse :
            self.clManager.startUpdatingLocation()
            break
        default :
            break
        }
    }

}
