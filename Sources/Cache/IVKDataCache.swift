//
//  IVKDataCache.swift
//  IVKLive
//
//  Created by Fred Priese on 7/11/16.
//  Copyright © 2016 Aisle411. All rights reserved.
//

import Foundation

internal class IVKDataCache: IVKObject {
    private static let cacheDirectoryFileName: String = "IVKDataCacheDirectory"
    private let cacheURL: URL
    private var venues: [Int: IVKDataCacheForVenue] = [:]
    
    internal init(rootCacheURL: URL) {
        self.cacheURL = rootCacheURL.appendingPathComponent(IVKDataCache.cacheDirectoryFileName)
        
        super.init()
        if let dict: NSDictionary = NSDictionary(contentsOf: self.cacheURL) {
            for (idString, nsDict) in dict {
                if let id: Int = Int(idString as! String) {
                    self.venues[id] = IVKDataCacheForVenue(venueId: id, dict: nsDict as! NSDictionary)
                }
            }
        }
    }
    
    internal func dataCacheForVenue(id: Int) -> IVKDataCacheForVenue {
        if let dCache = venues[id] {
            return dCache
        }
        // add empty cache and return it.
        self.venues[id] = IVKDataCacheForVenue(venueId: id, dict: [:])
        return self.venues[id]!
    }
    
    internal func updateDataCacheForVenue(dCache: IVKDataCacheForVenue) {
        if let _ = self.venues[dCache.venueId] {
            self.venues.removeValue(forKey: dCache.venueId)
        }
        self.venues[dCache.venueId] = dCache
        self.cache()
    }
    
    private func cache() {
        var dict: [String: AnyObject] = [:]
        for (id, venue) in self.venues {
            dict["\(id)"] = venue.dict()
        }
        
        let nsDict: NSDictionary = dict as NSDictionary
        if !nsDict.write(to: cacheURL, atomically: true) {
            babelfish.severe("Caching of directory of last modified dates for binary API results failed.")
        }
    }
}

internal class IVKDataCacheForVenue: IVKObject {
    private let kIMAP: String = "imap"
    private let kMPX: String = "mpx"
    private let kMPXSS: String = "mpxss"
    private let kSSQLITE: String = "suggestionsqlite"
    private let kSTEXT: String = "suggestiontext"
    
    internal let venueId: Int
    internal let imapEntry: IVKDataCacheEntry
    internal let mpxEntry: IVKDataCacheEntry
    internal let mpxStyleSheetEntry: IVKDataCacheEntry
    internal let searchSuggestionEntry: IVKDataCacheEntry
    internal let searchSuggestionTextEntry: IVKDataCacheEntry
    
    internal init(venueId: Int, dict: NSDictionary) {
        self.venueId = venueId
        let dawn: Date = Date(timeIntervalSince1970: 0.0)
        
        func lMDForEntryFromDict(dict: NSDictionary, entryKey: String, dateKey: String) -> Date {
            if let entryDict: NSDictionary = dict[entryKey] as? NSDictionary {
                if let lMD: Date = entryDict[dateKey] as? Date {
                    return lMD
                }
            }
            return dawn
        }
        
        var workingEntry: IVKDataCacheEntry?
        
        if dict.count > 0 {
            if let entryDict: NSDictionary = dict[kIMAP] as? NSDictionary {
                if let entry = IVKDataCacheEntry(dict: entryDict) {
                    workingEntry = entry
                }
            }
            if let _ = workingEntry {
                self.imapEntry = workingEntry!
            } else {
                self.imapEntry = IVKDataCacheEntry(entryName: kIMAP, lastModifiedDate: dawn)
            }
            workingEntry = nil
            
            if let entryDict: NSDictionary = dict[kMPX] as? NSDictionary {
                if let entry = IVKDataCacheEntry(dict: entryDict) {
                    workingEntry = entry
                }
            }
            if let _ = workingEntry {
                self.mpxEntry = workingEntry!
            } else {
                self.mpxEntry = IVKDataCacheEntry(entryName: kMPX, lastModifiedDate: dawn)
            }
            workingEntry = nil
            
            if let entryDict: NSDictionary = dict[kMPXSS] as? NSDictionary {
                if let entry = IVKDataCacheEntry(dict: entryDict) {
                    workingEntry = entry
                }
            }
            if let _ = workingEntry {
                self.mpxStyleSheetEntry = workingEntry!
            } else {
                self.mpxStyleSheetEntry = IVKDataCacheEntry(entryName: kMPXSS, lastModifiedDate: dawn)
            }
            workingEntry = nil
            
            if let entryDict: NSDictionary = dict[kSSQLITE] as? NSDictionary {
                if let entry = IVKDataCacheEntry(dict: entryDict) {
                    workingEntry = entry
                }
            }
            if let _ = workingEntry {
                self.searchSuggestionEntry = workingEntry!
            } else {
                self.searchSuggestionEntry = IVKDataCacheEntry(entryName: kSSQLITE, lastModifiedDate: dawn)
            }
            workingEntry = nil
            
            if let entryDict: NSDictionary = dict[kSTEXT] as? NSDictionary {
                if let entry = IVKDataCacheEntry(dict: entryDict) {
                    workingEntry = entry
                }
            }
            if let _ = workingEntry {
                self.searchSuggestionTextEntry = workingEntry!
            } else {
                self.searchSuggestionTextEntry = IVKDataCacheEntry(entryName: kSTEXT, lastModifiedDate: dawn)
            }
            workingEntry = nil
        } else {
            self.imapEntry = IVKDataCacheEntry(entryName: kIMAP, lastModifiedDate: dawn)
            self.mpxEntry = IVKDataCacheEntry(entryName: kMPX, lastModifiedDate: dawn)
            self.mpxStyleSheetEntry = IVKDataCacheEntry(entryName: kMPXSS, lastModifiedDate: dawn)
            self.searchSuggestionEntry = IVKDataCacheEntry(entryName: kSSQLITE, lastModifiedDate: dawn)
            self.searchSuggestionTextEntry = IVKDataCacheEntry(entryName: kSTEXT, lastModifiedDate: dawn)
        }
        super.init()
    }
    
    internal func dict() -> NSDictionary {
        var dict: [String: AnyObject] = [:]
        
        dict[kIMAP] = self.imapEntry.dict()
        dict[kMPX] = self.mpxEntry.dict()
        dict[kMPXSS] = self.mpxStyleSheetEntry.dict()
        dict[kSSQLITE] = self.searchSuggestionEntry.dict()
        dict[kSTEXT] = self.searchSuggestionTextEntry.dict()
        return dict as NSDictionary
    }
}

internal class IVKDataCacheEntry: IVKObject {
    private let kEntryName: String = "entryName"
    private let kLastModifiedDate: String = "lastModifiedDate"
    internal let entryName: String
    internal var lastModifiedDate: Date
    
    internal func dict() -> NSDictionary {
        var dict: [String: AnyObject] = [:]
        dict[kEntryName] = self.entryName as AnyObject?
        dict[kLastModifiedDate] = self.lastModifiedDate as AnyObject?
        return dict as NSDictionary
    }
    
    internal init(entryName: String, lastModifiedDate: Date) {
        self.entryName = entryName
        self.lastModifiedDate = lastModifiedDate
        super.init()
    }
    
    internal init?(dict: NSDictionary) {
        guard let entryName: String = dict[kEntryName] as? String else {
            return nil
        }
        
        guard let lastModifiedDate: Date = dict[kLastModifiedDate] as? Date else {
            return nil
        }
        
        self.entryName = entryName
        self.lastModifiedDate = lastModifiedDate
        super.init()
    }
}
